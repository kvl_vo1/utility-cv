const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");

const options = (env, argv) => {
  const SourceDir = path.join(__dirname, "src");
  const BuildDir = path.join(__dirname, "build");

  const isDev = argv.mode === "development";

  const dropMinSuffix = (items, isDev) => {
    if (!isDev) return items;

    const dropMinStr = s => s.replace(".min.", ".");

    let res;

    if (typeof items === "string") {
      res = dropMinStr(items)
    } else if (Array.isArray(items)) {
      res = items.map(item => dropMinStr(item))
    } else {
      throw "Input param 'items' is not a string or array"
    }

    return res;
  };

  const getGlobOptions = (ip = ["**/*.min.*"]) => {
    return {
      ignore: isDev ? [...ip] : [],
    }
  };

  return {
    entry: [
      "babel-polyfill",
      path.join(SourceDir, "/index.js")
    ],

    devtool: isDev ? "source-map" : "",

    output: {
      path: BuildDir,
      filename: "app.js",
      publicPath: "/"
    },

    module: {
      rules: [
        {
          test: /\.js$/,
          include: SourceDir,
          use: {
            loader: "babel-loader",
            options: {
              cacheDirectory: true,
              presets: ["@babel/react", "@babel/env"],
              plugins: [/*'react-hot-loader/babel',*/ "transform-class-properties", "@babel/proposal-class-properties", "transform-regenerator"]
            }
          }
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            "css-hot-loader",
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                // only enable hot in development
                hmr: isDev,
                // if hmr does not work, this is a forceful method.
                reloadAll: true,
              },
            },
            "css-loader",
            "sass-loader"
          ],
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "[path][name].[ext]",
                context: "src/assets/fonts/",
                outputPath: "assets/fonts/",
                useRelativePath: true
              }
            },
          ]
        },
        {
          test: /\.(png|jp(e*)g)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "[path][name].[ext]",
                context: "src/images/",
                outputPath: "images/",
                useRelativePath: true
              }
            },
          ]
        },
      ]
    },

    devServer: {
      host: "localhost",
      port: 3000,
      disableHostCheck: true,
      contentBase: SourceDir,
      watchContentBase: true,
      historyApiFallback: true,
      compress: true,
      stats: "errors-only", //"normal",
      liveReload: true,
      open: true,
      inline: true,
      hot: true
    },

    plugins: [
      new webpack.DefinePlugin({
        "process.env.SERVER": JSON.stringify(
          isDev ? "http://localhost:9000/api" : "https://utility.hosting.com/api"
        ),
      }),

      new HtmlWebpackPlugin({
        title: "Комунальні платежі",
        rs: "/styles/reset.css",
        jq: dropMinSuffix("/assets/jquery/jquery.min.js", isDev),
        bs: dropMinSuffix(["/assets/bootstrap/css/bootstrap.min.css", "/assets/bootstrap/bootstrap.min.js"], isDev),
        sl: dropMinSuffix(["/assets/owl/assets/owl.carousel.min.css", "/assets/owl/assets/owl.theme.default.min.css", "/assets/owl/owl.carousel.min.js"], isDev),
        mt: dropMinSuffix(["/assets/materialize-css/css/materialize.min.css", "/assets/materialize-css/materialize.min.js"], isDev),
        fa: dropMinSuffix("/assets/fontawesome/css/all.min.css", isDev),
        bu: dropMinSuffix("/assets/bulma/bulma.min.css", isDev),
        // minify: {
        //     collapseWhitespace: true
        // },
        // hash: true,
        template: path.join(SourceDir, "index.html")
      }),

      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // all options are optional
        filename: "styles/style.css",
        chunkFilename: "[id].css",
        // Enable to remove warnings about conflicting order
        ignoreOrder: false,
      }),

      new CopyWebpackPlugin({
        patterns: [
          {
            context: "src/images",
            from: "**/*.png",
            to: path.join(BuildDir, "images"),
            noErrorOnMissing: true,
          },
          {
            context: "src/images",
            from: "**/*.jp*g",
            to: path.join(BuildDir, "images"),
            noErrorOnMissing: true,
          },
          {
            context: "src/style/fonts",
            from: "**/*.*",
            to: path.join(BuildDir, "styles/fonts"),
            noErrorOnMissing: true,
          },

          {
            context: "node_modules/bootstrap/dist/js",
            from: dropMinSuffix("bootstrap.min.js", isDev),
            to: path.join(BuildDir, "assets/bootstrap")
          },
          {
            context: "node_modules/bootstrap/dist/fonts",
            from: "*.*",
            to: path.join(BuildDir, "assets/bootstrap/fonts")
          },
          {
            context: "node_modules/bootstrap/dist/css",
            from: dropMinSuffix("bootstrap.min.css", isDev),
            to: path.join(BuildDir, "assets/bootstrap/css")
          },

          {
            context: "node_modules/jquery/dist/",
            from: dropMinSuffix("jquery.min.js", isDev),
            to: path.join(BuildDir, "assets/jquery")
          },

          {
            context: "node_modules/owl.carousel/dist/",
            from: dropMinSuffix("owl.carousel.min.js", isDev),
            to: path.join(BuildDir, "assets/owl")
          },
          {
            context: "node_modules/owl.carousel/dist/assets/",
            from: "*.*",
            to: path.join(BuildDir, "assets/owl/assets"),
            globOptions: getGlobOptions(),
          },

          {
            context: "node_modules/reset-css/",
            from: "reset.css",
            to: path.join(BuildDir, "styles")
          },

          {
            context: "node_modules/materialize-css/dist/css/",
            from: dropMinSuffix("materialize.min.css", isDev),
            to: path.join(BuildDir, "assets/materialize-css/css"),
          },
          {
            context: "node_modules/materialize-css/dist/js/",
            from: dropMinSuffix("materialize.min.js", isDev),
            to: path.join(BuildDir, "assets/materialize-css/"),
          },

          {
            context: "src/assets/fontawesome-pro-5.13.0/css/",
            from: dropMinSuffix("*.min.css", isDev),
            to: path.join(BuildDir, "assets/fontawesome/css"),
          },
          {
            context: "src/assets/fontawesome-pro-5.13.0/webfonts/",
            from: "*.*",
            to: path.join(BuildDir, "assets/fontawesome/webfonts"),
          },

          {
            context: "src/assets/fonts/",
            from: "**/*.*",
            to: path.join(BuildDir, "assets/fonts"),
          },

          {
            context: "node_modules/bulma/css",
            from: dropMinSuffix("*.min.css", isDev),
            to: path.join(BuildDir, "assets/bulma"),
          },
        ]
      }),

      new webpack.HotModuleReplacementPlugin()
    ]
  }
};

module.exports = options;