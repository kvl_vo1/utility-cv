import axios from "axios";
import {useThunkReducer} from "./thunk-reducer";

const TYPE_INT = "TYPE_INT";
const TYPE_FLOAT = "TYPE_FLOAT";

const PHONE = "PHONE";
const INTERNET = "INTERNET";
const WATER = "WATER";
const INTERCOM = "INTERCOM";
const ELECTRICITY = "ELECTRICITY";
const HEATING = "HEATING";

function httpClient(host = process.env.SERVER) {
  return axios.create({
    baseURL: host,
    headers: {
      "Content-Type": "application/json"
    },
    timeout: 30 * 1000,
  });
}

const getBearer = token => {
  return `Bearer ${token}`
};

const getReceiptTypes = () => {
  return [ELECTRICITY, PHONE, WATER, HEATING, INTERCOM, INTERNET]
};

const getReceiptMeta = type => {
  let res = null;
  const total = "total";

  switch (type) {
    case PHONE:
      res = {
        root: "phone",
        name: "Телефон",
        icon: "fa-phone",
        fields: [
          {
            label: "Нарахування",
            placeholder: "",
            field: total,
            fieldType: TYPE_FLOAT
          }
        ]
      };
      break;

    case INTERNET:
      res = {
        root: "internet",
        name: "Інтернет",
        icon: "fa-globe",
        fields: [
          {
            label: "Нарахування",
            placeholder: "",
            field: total,
            fieldType: TYPE_FLOAT
          }
        ]
      };
      break;

    case WATER:
      res = {
        root: "utility",
        name: "Вода",
        icon: "fa-faucet-drip",
        fields: [
          {
            label: "",
            placeholder: "",
            field: "coldWater/bathroom",
            fieldType: TYPE_INT
          },
          {
            label: "",
            placeholder: "",
            field: "coldWater/kitchen",
            fieldType: TYPE_INT
          },
          {
            label: "",
            placeholder: "",
            field: "hotWater/bathroom",
            fieldType: TYPE_INT
          },
          {
            label: "",
            placeholder: "",
            field: "hotWater/kitchen",
            fieldType: TYPE_INT
          }
        ]
      };
      break;

    case INTERCOM:
      res = {
        root: "intercom",
        name: "Домофон",
        icon: "fa-bell",
        fields: [
          {
            label: "Нарахування",
            placeholder: "",
            field: total,
            fieldType: TYPE_FLOAT
          }
        ]
      };
      break;

    case ELECTRICITY:
      res = {
        root: "electricity",
        name: "Електроенергія",
        icon: "fa-bolt",
        fields: [
          {
            label: "Нарахування",
            placeholder: "",
            field: total,
            fieldType: TYPE_FLOAT
          },
          {
            label: "Показник лічильника",
            placeholder: "",
            field: "value",
            fieldType: TYPE_INT
          }
        ]
      };
      break;

    case HEATING:
      res = {
        root: "utility",
        name: "Опалення",
        icon: "fa-fireplace",
        fields: [
          {
            label: "Нарахування",
            placeholder: "",
            field: `heating/${total}`,
            fieldType: TYPE_FLOAT
          },
          {
            label: "Тариф за 1 Гкал",
            placeholder: "",
            field: "heating/price",
            fieldType: TYPE_FLOAT
          },
          {
            label: "Споживання, Гкал",
            placeholder: "",
            field: "heating/value",
            fieldType: TYPE_FLOAT
          }
        ]
      };
      break;

    default:
      res = null;
  }

  if (res) {
    res.getTotalFieldPath = () => {
      let fp = "";

      if (type === WATER) {
        let wf = {};

        res.fields.map(item => {
          const fl = item.field.split("/");
          const key = `${res.root}/${fl[0]}`;

          wf[key] = wf[key] ? [...wf[key], fl[1]] : [fl[1]]
        });

        fp = JSON.stringify(wf);
      } else {
        const f = res.fields.find(item => {
          return (item.field === total) || (item.field.indexOf(`/${total}`)) >= 0
        });

        if (f) {
          return `${res.root}/${f.field}`
        }
      }

      return fp;
    }
  }

  return res;
};

const getMonthMeta = month => {
  switch (month) {
    case 1:
      return {name: "Січень", image: "/images/monthes/winter/winter_01.jpg"};
    case 2:
      return {name: "Лютий", image: "/images/monthes/winter/winter_04.jpg"};

    case 3:
      return {name: "Березень", image: "/images/monthes/spring/spring_05.jpg"};
    case 4:
      return {name: "Квітень", image: "/images/monthes/spring/spring_02.jpg"};
    case 5:
      return {name: "Травень", image: "/images/monthes/spring/spring_03.jpg"};

    case 6:
      return {name: "Червень", image: "/images/monthes/summer/summer_01.jpg"};
    case 7:
      return {name: "Липень", image: "/images/monthes/summer/summer_02.jpg"};
    case 8:
      return {name: "Серпень", image: "/images/monthes/summer/summer_03.jpg"};

    case 9:
      return {name: "Вересень", image: "/images/monthes/autumn/autumn_04.jpg"};
    case 10:
      return {name: "Жовтень", image: "/images/monthes/autumn/autumn_02.jpg"};
    case 11:
      return {name: "Листопад", image: "/images/monthes/autumn/autumn_01.jpg"};

    case 12:
      return {name: "Грудень", image: "/images/monthes/winter/winter_03.jpg"};
  }
};

function isValidFloat(str) {
  str = String(str);
  return isNaN(str) || str.trim() === "" ? false : (/^[-+]?[\d]*(\.[\d]+)?$/g).test(str);
}

function isValidInteger(str) {
  str = String(str);
  return isNaN(str) || str.trim() === "" ? false : (/^[-+]?[\d]*(\.[0]+)?$/g).test(str);
}

const checkFieldTypeAndRange = (type, chkVal) => {
  const errMsg = "Помилка у введеному значенні. Допустимі символи:";

  chkVal = String(chkVal);
  let res = null;

  switch (type) {
    case TYPE_INT:
      res = isValidInteger(chkVal)
        ? {val: parseInt(chkVal), error: null}
        : {val: chkVal, error: `${errMsg} +-0123456789`};
      break;

    case TYPE_FLOAT:
      res = isValidFloat(chkVal)
        ? {val: parseFloat(chkVal), error: null}
        : {val: chkVal, error: `${errMsg} +-.0123456789`};
      break;

    default:
      res = {val: chkVal, error: `Тип даних "${type}" не знайдено!`};
  }

  return res;
};

const makeClassesStr = className => {
  if (Array.isArray(className)) {
    return className.join(" ")
  }

  return className
};

const unixToLocaleTimestamp = (unixtime, locale = "uk-UK") => {
  return new Date(unixtime).toLocaleString(locale)
};

export {PHONE, INTERNET, WATER, INTERCOM, ELECTRICITY, HEATING, TYPE_INT, TYPE_FLOAT};
export {httpClient, getBearer, getReceiptTypes, getReceiptMeta, getMonthMeta};
export {isValidFloat, isValidInteger, checkFieldTypeAndRange, makeClassesStr};
export {useThunkReducer};
export {unixToLocaleTimestamp};