import React, {useContext} from "react";
import PropTypes from "prop-types";
import {Redirect, Route} from "react-router-dom";
import AppCtx from "../context/AppCtx";

function PrivateRoute({component: Component, redirectTo, ...rest}) {
  const {isAuthenticated} = useContext(AppCtx);

  return (
    <Route
      {...rest}
      render={props => isAuthenticated()
        ? <Component {...props}/>
        : <Redirect to={redirectTo}/>
      }/>
  );
}

PrivateRoute.defaultProps = {
  redirectTo: "/signin"
};

PrivateRoute.propTypes = {
  redirectTo: PropTypes.string.isRequired,
  component: PropTypes.any.isRequired
};

export default PrivateRoute;