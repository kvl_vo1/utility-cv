import React, {useEffect} from "react";
import PropTypes from "prop-types";
import jwt_decode from "jwt-decode";
import produce from "immer";
import AppCtx from "./AppCtx";
import {ERROR_MSG, IN_PROGRESS, IS_INITIALIZED, LOAD_RECEIPTS, MODAL, SET_RECEIPT, SIGNIN, SIGNOUT} from "./types";
import {appReducer, initialState} from "./appReducer";
import {getBearer, httpClient, useThunkReducer} from "../common";
import {PageMessage} from "../components";

function AppCtxWrapper({modalNode, children}) {
  const [state, dispatch] = useThunkReducer(appReducer, initialState, () => {
    return produce(initialState, draft => {
      draft.modal.node = modalNode;

      return draft;
    })
  });

  const JWT_LS_KEY = "jwt_utility";

  useEffect(() => {
    const token = getToken();

    if (token) {
      const res = parseToken(token);

      if (res) {
        dispatch({
          type: SIGNIN,
          payload: {
            email: res.email,
            fullName: res.fullName,
            isAdmin: res.isAdmin,
            expireAt: res.expireAt,
            token
          }
        });

        dispatch(setInitMode());
      } else {
        signOut()
      }
    } else {
      dispatch(setInitMode());
    }
  }, []);

  const acSetModal = data => {
    return dispatch => {
      if (data) {
        window.scrollTo(0, 0);
      }

      dispatch({
        type: MODAL,
        payload: data
      });
    }
  };

  const setModal = data => {
    dispatch(acSetModal(data))
  };

  const setInitMode = () => {
    return dispatch => {
      setError("");

      setTimeout(() => {
        dispatch({
          type: IS_INITIALIZED,
          payload: true
        });
      }, 10);
    }
  };

  const acAddNewPeriod = (history, cb) => {
    return () => {
      setError("");

      httpClient().post("/receipt/new", null, {
        headers: {
          Authorization: getBearer(state.auth.token)
        }
      })
        .then(response => {
          const gotoMonth = (h, d) => {
            return () => {
              h.push(`/month/${d}`)
            }
          };

          setReceipts(gotoMonth(history, response.data));
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
          setInProgress(false);
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const addNewPeriod = (history, cb) => {
    dispatch(acAddNewPeriod(history, cb))
  };

  const setError = text => {
    dispatch({type: ERROR_MSG, payload: text});
  };

  const acSetReceipts = cb => {
    return dispatch => {
      setError("");

      httpClient().get("/receipts", {
        headers: {
          Authorization: getBearer(state.auth.token)
        }
      })
        .then(response => {
          dispatch({type: LOAD_RECEIPTS, payload: response.data});
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const setReceipts = cb => {
    dispatch(acSetReceipts(cb));
  };

  const acSetReceipt = periodId => {
    return (dispatch, getState) => {
      setError("");

      if (periodId === 0) {
        dispatch({type: SET_RECEIPT, payload: null})
      } else if (periodId > 202000) {
        const state = getState();
        const receipt = state.receipts.find(r => r.periodId == periodId);

        if (receipt) {
          dispatch({type: SET_RECEIPT, payload: receipt})
        }
      }
    }
  };

  const setReceipt = periodId => {
    dispatch(acSetReceipt(periodId));
  };

  const acUpdateReceipt = (field, value) => {
    return (dispatch, getState) => {
      setError("");

      let receipt = getState().receipt;

      if (receipt) {
        receipt = produce(receipt, r => {
          const fields = field.split("/");

          let v = r;
          for (let fld of fields) {
            v = v[fld];
          }

          if (Array.isArray(v)) {
            if (Array.isArray(value)) {
              v = value
            } else {
              v[0] = value
            }
          } else {
            v = value
          }

          let t = r;
          for (let i = 0; i < fields.length - 1; i++) {
            t = t[fields[i]];
          }
          // last field of object
          t[fields[fields.length - 1]] = v;
        });

        dispatch({type: SET_RECEIPT, payload: receipt})
      }
    }
  };

  const updateReceipt = (field, value) => {
    dispatch(acUpdateReceipt(field, value));
  };

  const acSaveReceipt = cb => {
    return (dispatch, getState) => {
      setError("");

      httpClient().post(
        `/receipt/${getState().receipt.periodId}`,
        JSON.stringify(getState().receipt),
        {
          headers: {
            Authorization: getBearer(getState().auth.token)
          }
        })
        .then(response => {
          //console.info(response.data)
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const saveReceipt = cb => {
    dispatch(acSaveReceipt(cb));
  };

  const setInProgress = inProgress => {
    dispatch({type: IN_PROGRESS, payload: inProgress});
  };

  const acSendReceipt = (periodId, cb) => {
    return (dispatch, getState) => {
      setError("");

      httpClient().post(
        `/send/${periodId}`,
        null,
        {
          headers: {
            Authorization: getBearer(state.auth.token)
          }
        })
        .then(response => {
          //console.info(response.data)
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const sendReceipt = (periodId, cb) => {
    dispatch(acSendReceipt(periodId, cb));
  };

  const acLoadReceipt = (periodId, cb) => {
    return (dispatch, getState) => {
      setError("");

      httpClient().get(
        `/receipt/${periodId}`,
        {
          headers: {
            Authorization: getBearer(getState().auth.token)
          }
        })
        .then(response => {
          dispatch({type: SET_RECEIPT, payload: response.data})
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const loadReceipt = (periodId, cb) => {
    dispatch(acLoadReceipt(periodId, cb));
  };

  const acSendFiles = (fd, cb) => {
    return (dispatch, getState) => {
      setError("");

      httpClient().post(
        "/files",
        fd,
        {
          headers: {
            Authorization: getBearer(state.auth.token)
          }
        })
        .then(response => {
          dispatch({type: SET_RECEIPT, payload: response.data});
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const sendFiles = (fd, cb) => {
    dispatch(acSendFiles(fd, cb));
  };

  const acRemoveFile = (data, cb) => {
    return (dispatch, getState) => {
      setError("");

      httpClient().post(
        "/files/remove",
        JSON.stringify(data),
        {
          headers: {
            Authorization: getBearer(state.auth.token)
          }
        })
        .then(response => {
          dispatch({type: SET_RECEIPT, payload: response.data});
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const removeFile = (data, cb) => {
    dispatch(acRemoveFile(data, cb));
  };

  const getToken = () => {
    return localStorage.getItem(JWT_LS_KEY) || ""
  };

  const setToken = token => {
    if (!token) {
      localStorage.removeItem(JWT_LS_KEY);
      return;
    }

    localStorage.setItem(JWT_LS_KEY, token);
  };

  const parseToken = token => {
    try {
      const jwt = jwt_decode(token);

      return {
        email: jwt["login"],
        fullName: jwt["name"],
        isAdmin: Boolean(jwt["isAdmin"]),
        expireAt: Number(jwt["exp"]) * 1000,
        token
      }
    } catch (e) {
      return null
    }
  };

  const isAuthenticated = () => {
    const res = parseToken(state.auth.token);
    return res ? res.expireAt > Date.now() : false;
  };

  const acSignOut = (isInitialized, cb) => {
    return (dispatch, getState) => {
      httpClient().get("/signout",
        {
          headers: {
            Authorization: getBearer(getState().auth.token)
          }
        })
        .then(response => {
          setToken("");

          dispatch({type: SIGNOUT, payload: isInitialized});
        })
        .catch(error => {
          if (error.response) {
            const {data, status, statusText} = error.response;
            setError(data ? data : `${status}: ${statusText}`);
          } else {
            setError(error.message);
          }
        })
        .finally(() => {
          if (cb) {
            cb()
          } else {
            setInProgress(false)
          }
        });
    }
  };

  const signOut = (isInitialized = false, cb = null) => {
    dispatch(acSignOut(isInitialized, cb));
  };

  return (
    <AppCtx.Provider value={{
      state, dispatch,
      signOut,
      setModal,
      setInProgress, setError,
      parseToken, setToken, isAuthenticated,
      addNewPeriod,
      setReceipts, setReceipt, updateReceipt, saveReceipt,
      loadReceipt,
      sendReceipt,
      sendFiles,
      removeFile
    }}>
      {
        state.common.isInitialized
          ? children
          : <PageMessage isError={false} message="Ініціалізація..."/>
      }
    </AppCtx.Provider>
  );
}

AppCtxWrapper.propTypes = {
  modalNode: PropTypes.instanceOf(HTMLDivElement).isRequired,
  children: PropTypes.any.isRequired
};

export default AppCtxWrapper;