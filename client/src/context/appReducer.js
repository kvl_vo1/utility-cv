import produce from "immer";
import {
  ERROR_MSG,
  IN_PROGRESS,
  IS_INITIALIZED,
  LOAD_RECEIPTS,
  MODAL,
  SET_RECEIPT,
  SIGNIN,
  SIGNOUT,
  SIGNUP,
} from "./types";

const initialState = {
  common: {
    isInitialized: false,
    inProgress: false,
    error: ""
  },
  modal: {
    node: null,
    children: null,
  },
  auth: {
    email: "",
    fullName: "",
    isAdmin: false,
    token: "",
    expireAt: 0
  },
  receipts: [],
  receipt: null
};

function appReducer(state = initialState, action) {
  switch (action.type) {
    case MODAL:
      return produce(state, draft => {
        draft.modal.children = action.payload;
        return draft;
      });

    case IS_INITIALIZED:
      return produce(state, draft => {
        draft.common.isInitialized = action.payload;
        return draft;
      });

    case IN_PROGRESS:
      return produce(state, draft => {
        draft.common.inProgress = action.payload;
        return draft;
      });

    case ERROR_MSG:
      return produce(state, draft => {
        draft.common.error = action.payload;
        return draft;
      });

    case SIGNUP:
      return state;

    case SIGNIN:
      const {email, fullName, isAdmin, expireAt, token} = action.payload;
      return produce(state, draft => {
        draft.auth = {email, fullName, isAdmin, expireAt, token};
        return draft;
      });

    case SIGNOUT:
      return produce(state, draft => {
        draft.common.isInitialized = action.payload;
        draft.common.inProgress = false;
        draft.common.error = "";

        draft.auth.email = "";
        draft.auth.fullName = "";
        draft.auth.isAdmin = false;
        draft.auth.token = "";
        draft.auth.expireAt = 0;

        draft.receipts = [];
        draft.receipt = null;

        return draft
      });

    case LOAD_RECEIPTS:
      return produce(state, draft => {
        draft.receipt = null;
        draft.receipts = action.payload;
        return draft;
      });

    case SET_RECEIPT:
      return produce(state, draft => {
        draft.receipt = action.payload;
        return draft;
      });

    default:
      return state;
  }
}

export {initialState, appReducer};
