import React from "react";
import PropTypes from "prop-types";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {AuthForm, Container, Dashboard, Receipts, PageMessage} from "./components";
import AppCtxWrapper from "./context/AppCtxWrapper";
import PrivateRoute from "./common/privateRoute";
import Modal from "./components/Modal/Modal";

import "./styles/styles.scss";

function App({modalNode}) {
  return (
    <AppCtxWrapper modalNode={modalNode}>
      <Modal/>
      <Container>
        <Router>
          <Switch>
            <PrivateRoute exact path="/" component={Dashboard}/>
            <PrivateRoute exact path="/month/:periodId" component={Receipts}/>
            <Route exact path="/signin" render={() => <AuthForm viewMode={true}/>}/>
            <Route exact path="/signup" render={() => <AuthForm viewMode={false}/>}/>
            <Route render={props => <PageMessage message="Сторінку не знайдено" {...props}/>}/>
          </Switch>
        </Router>
      </Container>
    </AppCtxWrapper>
  );
}

App.propTypes = {
  modalNode: PropTypes.instanceOf(HTMLDivElement).isRequired
};

export default App;
