import React from "react";
import ReactDOM from "react-dom";
import App from "./app";

const root = document.getElementById("root");
const modal = document.getElementById("modal");
ReactDOM.render(<App modalNode={modal}/>, root);

if (module.hot) {
  module.hot.accept();
}
