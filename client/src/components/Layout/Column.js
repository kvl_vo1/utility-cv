import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {makeClassesStr} from "../../common";

function Column({children, className}) {
  const cl = classNames("column", makeClassesStr(className));

  return (
    <div className={cl}>
      {children}
    </div>
  );
}

Column.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
};

export default Column;