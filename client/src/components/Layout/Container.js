import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {makeClassesStr} from "../../common";

function Container({children, className}) {
  const cl = classNames("container", makeClassesStr(className));

  return (
    <div className={cl}>
      {children}
    </div>
  );
}

Container.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
};

export default Container;