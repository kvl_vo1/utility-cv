import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {makeClassesStr} from "../../common";

function LevelSide({children, className}) {
  const cl = classNames(makeClassesStr(className));

  return (
    <div className={cl}>
      {children}
    </div>
  );
}

LevelSide.defaultProps = {
  children: ""
};

LevelSide.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
};

export default LevelSide;