import React from "react";
import {Panel} from "./index";

function Progress() {
  return (
    <Panel className="ch-progress">
      <div className="back-drop"/>
      <span className="icon is-large ch-align-10">
        <i className="fal fa-spinner-third fa-6x ch-pulse"></i>
      </span>
    </Panel>
  );
}

export default Progress;