import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {makeClassesStr} from "../../common";

function Panel({children, className}) {
  const cl = classNames("panel", makeClassesStr(className));

  return (
    <div className={cl}>
      {children}
    </div>
  );
}

Panel.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
};

export default Panel;