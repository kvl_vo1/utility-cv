import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import {makeClassesStr} from "../../common";

function Row({children, className}) {
  const cl = classNames("columns", makeClassesStr(className));

  return (
    <div className={cl}>
      {children}
    </div>
  );
}

Row.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
};

export default Row;