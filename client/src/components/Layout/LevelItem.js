import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {makeClassesStr} from "../../common";

function LevelItem({children, className}) {
  const cl = classNames("level-item", makeClassesStr(className));

  return (
    <div className={cl}>
      {children}
    </div>
  );
}

LevelItem.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
};

export default LevelItem;