import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {makeClassesStr} from "../../common";

function Level({children, className}) {
  const cl = classNames("level", makeClassesStr(className));

  return (
    <div className={cl}>
      {children}
    </div>
  );
}

Level.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
};

export default Level;