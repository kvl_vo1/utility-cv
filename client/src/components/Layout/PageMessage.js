import React from "react";
import PropTypes from "prop-types";
import {Column, Row, Level, LevelSide, LevelItem} from "./index";

function PageMessage(props) {
  const {history, isError, message} = props;

  const goToHome = () => {
    history.replace("/");
  };

  const icon = isError ? "fa-siren-on" : "fa-clock";
  const color = isError ? "is-danger" : "is-info";

  return (
    <Row className="is-block">
      <Column className="is-6 is-offset-3">
        <Column className="is-12">
          <div className={`notification py-3 px-3 ${color}`}>
            <Level className="is-flex">
              <LevelSide className="level-left">
                <LevelItem>
                  <p className="is-size-3">
                    <i className={`fal mr-3 ${icon}`}></i>
                    {message}
                  </p>
                </LevelItem>
              </LevelSide>
              <LevelSide className="level-right">
                {
                  isError &&
                  <LevelItem>
                    <button className="button has-background-warning-light px-3" onClick={goToHome}>
                    <span className="icon is-small">
                      <i className="fal fa-home"></i>
                    </span>
                      <span>На головну</span>
                    </button>
                  </LevelItem>
                }
              </LevelSide>
            </Level>
          </div>
        </Column>
      </Column>
    </Row>
  );
}

PageMessage.defaultProps = {
  isError: true
};

PageMessage.propTypes = {
  isError: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  history: PropTypes.object,
};

export default PageMessage;