import React, {useContext} from "react";
import PropTypes from "prop-types";
import AppCtx from "../../context/AppCtx";
import {msgConfirm, settingsForm} from "../Modal/Modal";
import {Level, LevelSide} from "../index";

function MainMenu({history}) {
  const ctx = useContext(AppCtx);
  const {signOut} = ctx;

  const onClickSettings = () => {
    settingsForm(ctx)
  };

  const onClickLogout = () => {
    const onConfirm = () => {
      signOut(true);
    };

    msgConfirm(ctx, <p>Вийти з акаунту?</p>, "Увага!", onConfirm);
  };

  return (
    <div className="panel-heading mb-3 has-background-info has-text-white ch-round">
      <Level>
        <LevelSide className="level-left">
          <p className="is-size-4">Комунальні платежі</p>
        </LevelSide>
        <LevelSide className="level-right">
          <button className="button is-info mr-3" onClick={onClickSettings}>
            <span className="icon">
              <i className="fal fa-cog is-size-4"></i>
            </span>
          </button>
          <button className="button is-info" onClick={onClickLogout}>
            <span className="icon">
              <i className="fal fa-sign-out is-size-4"></i>
            </span>
          </button>
        </LevelSide>
      </Level>
    </div>
  );
}

MainMenu.propTypes = {
  history: PropTypes.object.isRequired
};

export default MainMenu;