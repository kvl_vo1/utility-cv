import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";

function AuthField({id, fields, setField}) {
  const [field, setFieldInfo] = useState({touched: false, value: "", error: ""});

  useEffect(() => {
    setFieldInfo(fields[id])
  }, [id, fields]);

  const onChange = e => {
    e.persist();
    setFieldInfo(old => {
      return {...old, value: e.target.value}
    })
  };

  const onBlur = () => {
    setField(id, field.value);
  };

  let data = {
    placeholder: "",
    icon: "",
    type: ""
  };

  switch (id) {
    case "name":
      data = {placeholder: "Ім'я", icon: "fa-user", type: "text"};
      break;

    case "email":
      data = {placeholder: "Електронна пошта", icon: "fa-envelope", type: "email"};
      break;

    case "password1":
      data = {placeholder: "Пароль", icon: "fa-lock", type: "password"};
      break;

    case "password2":
      data = {placeholder: "Пароль повторно", icon: "fa-lock", type: "password"};
      break;
  }

  const hasError = field.touched && field.error.trim() !== "";

  return (
    <div className="field">
      <div className="control has-icons-left has-icons-right mb-2">
        <input
          id={id}
          className="input is-info"
          type={data.type}
          placeholder={data.placeholder}
          onChange={onChange}
          onBlur={onBlur}
          value={field.value}
          autoComplete="new-password"
        />
        <span className="icon is-small is-left">
          <i className={`fal ${data.icon}`}/>
        </span>
        <span className={`icon is-small is-right ${field.touched ? "" : "ch-invisible"}`}>
          <i className={`fal ${hasError ? "fa-exclamation is-danger" : "fa-check is-primary"}`}/>
        </span>
      </div>
      <p className={`help is-danger ${hasError ? "" : "is-hidden"}`}>{field.error}</p>
    </div>
  );
}

AuthField.propTypes = {
  id: PropTypes.oneOf(["name", "email", "password1", "password2"]).isRequired,
  fields: PropTypes.object.isRequired,
  setField: PropTypes.func.isRequired
};

export default AuthField;