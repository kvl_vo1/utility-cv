import React, {useContext, useEffect, useRef, useState} from "react";
import PropTypes from "prop-types";
import {useHistory} from "react-router-dom";
import produce from "immer";
import AuthField from "./AuthField";
import {httpClient} from "../../common";
import AppCtx from "../../context/AppCtx";
import {SIGNIN} from "../../context/types";
import {Level, LevelItem, LevelSide} from "../index";

function AuthForm({viewMode}) {
  const [isSignInMode, setSignInMode] = useState(viewMode);
  const [inProgress, setInProgress] = useState(false);
  const [message, setMessage] = useState({color: "", text: ""});
  const [fields, setFields] = useState({
    name: {touched: false, value: "", error: "", signInOnly: false},
    email: {touched: false, value: "", error: "", signInOnly: true},
    password1: {touched: false, value: "", error: "", signInOnly: true},
    password2: {touched: false, value: "", error: "", signInOnly: false},
  });

  const history = useHistory();

  const {state, dispatch, parseToken, setToken, isAuthenticated} = useContext(AppCtx);

  useEffect(() => {
    if (!inProgress && isAuthenticated()) {
      history.replace("/");
    }
  }, [state, inProgress]);

  const btnSubmit = useRef(null);

  const setField = (id, value) => {
    const re = /^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
    let error = "";
    let touched = true;

    switch (id) {
      case "name":
        error = value.trim().length >= 3 && value.trim().length <= 64 ? "" : "Поле повинно містити 3-64 символи.";
        break;

      case "email":
        if (value === "admin") break;
        error = re.test(String(value)) ? "" : "Невірна електронна пошта.";
        break;

      case "password1":
        error = value.trim().length >= 4 && value.trim().length <= 16 ? "" : "Поле повинно містити 4-16 символів.";
        break;

      case "password2":
        error = value.trim().length >= 4 && value.trim().length <= 16 ? "" : "Поле повинно містити 4-16 символів.";

        if (!isSignInMode && fields["password1"].value) {
          error = value === fields["password1"].value ? "" : "Паролі не співпадають."
        }

        break;
    }

    setFields(state => {
      return {
        ...state,
        [id]: {...state[id], value, error, touched}
      }
    });

    return error.trim() === "";
  };

  const onKeyDown = e => {
    if (e.keyCode === 13) {
      btnSubmit.current.focus();
    }
  };

  const onSubmit = e => {
    e.preventDefault();

    if (!validateFields()) {
      return
    }

    setMessage({color: "", text: ""});
    setInProgress(true);

    const data = JSON.stringify({
      "login": fields["email"].value,
      "fullName": fields["name"].value,
      "password": fields["password1"].value,
    });

    httpClient().post(`${isSignInMode ? "/signin" : "/signup"}`, data)
      .then(response => {
        if (!isSignInMode) {
          setMessage({color: "", text: "Користувач успішно зареєстрований. Будь-ласка ввійдіть в систему."});
          setSignInMode(true);
        } else {
          try {
            const jwt = parseToken(response.data);

            if (jwt) {
              const {email, fullName, isAdmin, expireAt, token} = jwt;

              dispatch({
                type: SIGNIN,
                payload: {email, fullName, isAdmin, expireAt, token}
              });

              setToken(token);
            }
          } catch (e) {
            throw new Error("Wrong authentication data!")
          }
        }

        clearFields();
      })
      .catch(error => {
        if (error.response) {
          const {data, status, statusText} = error.response;

          setMessage({
            color: [200, 202, 204].includes(status) ? "is-primary" : "is-danger",
            text: data ? data : `${status}: ${statusText}`
          });
        } else {
          setMessage({color: "is-danger", text: error.message});
        }
      })
      .finally(() => {
        setInProgress(false);
      });
  };

  const onBtnClick = () => {
    setMessage({color: "", text: ""});
    clearFields();
    setSignInMode(state => !state);
  };

  const validateFields = () => {
    let res = true;

    for (let key of Object.keys(fields)) {
      if (isSignInMode) {
        res &= fields[key].signInOnly ? setField(key, fields[key].value) : true;
      } else {
        res &= setField(key, fields[key].value)
      }
    }

    return res;
  };

  const clearFields = () => {
    const clearField = (o) => {
      for (let key of Object.keys(o)) {
        if (key !== "signInOnly") {
          switch (typeof o[key]) {
            case "string":
              o[key] = "";
              break;

            case "boolean":
              o[key] = false;
              break;

            case "number":
              o[key] = 0;
              break;

            case "object":
              clearField(o[key]);
              break;
          }
        }
      }
    };

    const f = produce(fields, draft => {
      clearField(draft)
    });

    setFields(f);
  };

  const btnSecondary = (isSignIn, caption, disabled) => {
    return (
      <button
        type="button"
        onClick={onBtnClick}
        className="button is-outlined px-4 has-text-grey-light"
        disabled={disabled}
      >
        <span className="icon is-small">
          <i className={`fal ${isSignIn ? "fa-sign-in" : "fa-user-plus"}`}/>
        </span>
        <span>{caption}</span>
      </button>
    )
  };

  const btnPrimary = (isSignIn, caption, disabled) => {
    return (
      <button
        ref={btnSubmit}
        type="submit"
        className={`button is-primary px-6 ${disabled ? "is-loading" : ""}`}
      >
        <span className="icon is-small">
          <i className={`fal ${isSignIn ? "fa-sign-in" : "fa-user-plus"}`}/>
        </span>
        <span>{caption}</span>
      </button>
    )
  };

  const getCaption = isSignIn => isSignIn ? "Вхід" : "Реєстрація";

  return (
    <div className="columns vh100 auth">
      <div className="column is-6 is-offset-3 auth-container">
        <Level className="auth-wrapper">
          <LevelItem>
            <form noValidate={true} className="box w100p" onSubmit={onSubmit} onKeyDown={onKeyDown}>
              {
                message.text && <div className={`notification mb-2 ${message.color ? message.color : "is-primary"}`}>
                  {message.text}
                </div>
              }
              <div className="notification is-info mb-5 has-text-centered fa-2x">
                <strong>{getCaption(isSignInMode)}</strong>
              </div>

              <fieldset className="mb-5" disabled={inProgress}>
                {!isSignInMode && <AuthField id="name" fields={fields} setField={setField}/>}
                <AuthField id="email" fields={fields} setField={setField}/>
                <AuthField id="password1" fields={fields} setField={setField}/>
                {!isSignInMode && <AuthField id="password2" fields={fields} setField={setField}/>}
              </fieldset>

              <Level className="is-flex">
                <LevelSide className="level-left">
                  <LevelItem>
                    {btnSecondary(!isSignInMode, getCaption(!isSignInMode), inProgress)}
                  </LevelItem>
                </LevelSide>
                <LevelSide className="level-right">
                  <LevelItem>
                    {btnPrimary(isSignInMode, getCaption(isSignInMode), inProgress)}
                  </LevelItem>
                </LevelSide>
              </Level>
            </form>
          </LevelItem>
        </Level>
      </div>
    </div>
  );
}

AuthForm.defaultProps = {
  viewMode: true
};

AuthForm.propTypes = {
  viewMode: PropTypes.bool
};

export default AuthForm;