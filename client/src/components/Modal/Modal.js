import React, {useContext} from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";
import AppCtx from "../../context/AppCtx";
import ModalWrapper from "./ModalWrapper";
import SettingsForm from "../Settings/SettingsForm";
import {Level, LevelItem, LevelSide} from "../index";

const Message = ({type, children}) => {
  return (
    <article className={`message ${type}`}>
      {children}
    </article>
  )
};

Message.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired
};

const Header = ({header, onClick}) => {
  return (
    <div className="message-header">
      <p>{header}</p>
      <button className="delete" aria-label="delete" onClick={onClick}/>
    </div>
  )
};

Header.propTypes = {
  header: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

const Button = ({type, onClick, caption}) => {
  return (
    <LevelItem>
      <button className={`button is-outlined ${type}`} onClick={onClick}>
        {caption}
      </button>
    </LevelItem>
  )
};

Button.propTypes = {
  type: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

const Buttons = ({children}) => {
  return (
    <Level className="px-3 py-3">
      <LevelSide className="level-left"/>
      <LevelSide className="level-right">
        {children}
      </LevelSide>
    </Level>
  )
};

Buttons.propTypes = {
  children: PropTypes.any.isRequired
};

function Modal() {
  const {state: {modal: {node, children}}} = useContext(AppCtx);

  let root = document.getElementsByTagName("html");
  root = root && root.length > 0 ? root[0] : null;

  if (children) {
    root && root.classList.add("ch-overflow-hidden")
  } else {
    root && root.classList.remove("ch-overflow-hidden")
  }

  return ReactDOM.createPortal(children, node);
}

const msgInfo = (context, msgText, header = "") => {
  const {setModal} = context;
  const type = "is-info";
  const onCancel = () => setModal(null);

  const body = (
    <ModalWrapper onClick={onCancel}>
      <Message type={type}>
        <Header header={header} onClick={onCancel}/>
        <div className="message-body">
          {msgText}
        </div>
        <Buttons>
          <Button type="is-primary" onClick={onCancel} caption="Гаразд"/>
        </Buttons>
      </Message>
    </ModalWrapper>
  );

  setModal(body);
};

const msgWarning = (context, msgText, header = "") => {
  const {setModal} = context;
  const type = "is-warning";
  const onCancel = () => setModal(null);

  const body = (
    <ModalWrapper onClick={onCancel}>
      <Message type={type}>
        <Header header={header} onClick={onCancel}/>
        <div className="message-body">
          {msgText}
        </div>
        <Buttons>
          <Button type="is-primary" onClick={onCancel} caption="Гаразд"/>
        </Buttons>
      </Message>
    </ModalWrapper>
  );

  setModal(body);
};

const msgError = (context, msgText, header = "") => {
  const {setModal} = context;
  const type = "is-danger";
  const onCancel = () => setModal(null);

  const body = (
    <ModalWrapper onClick={onCancel}>
      <Message type={type}>
        <Header header={header} onClick={onCancel}/>
        <div className="message-body">
          {msgText}
        </div>
        <Buttons>
          <Button type="is-primary" onClick={onCancel} caption="Гаразд"/>
        </Buttons>
      </Message>
    </ModalWrapper>
  );

  setModal(body);
};

const msgConfirm = (context, msgText, header = "", onConfirm = null) => {
  const {setModal} = context;
  const type = "is-warning";
  const onCancel = () => setModal(null);
  const onConfEx = () => {
    setModal(null);
    if (onConfirm) onConfirm();
  };

  const body = (
    <ModalWrapper onClick={onCancel}>
      <Message type={type}>
        <Header header={header} onClick={onCancel}/>
        <div className="message-body">
          {msgText}
        </div>
        <Buttons>
          <Button type="is-danger" onClick={onCancel} caption="Ні"/>
          <Button type="is-primary" onClick={onConfEx} caption="Так"/>
        </Buttons>
      </Message>
    </ModalWrapper>
  );

  setModal(body);
};

const settingsForm = context => {
  const {setModal} = context;

  const onCancel = () => {
    setModal(null);
  };

  const body = (
    <ModalWrapper onClick={onCancel}>
      <Message type="is-info">
        <Header header="Налаштування" onClick={onCancel}/>
        <SettingsForm onCancel={onCancel}/>
      </Message>
    </ModalWrapper>
  );

  setModal(body);
};

export default Modal;
export {msgInfo, msgError, msgWarning, msgConfirm, settingsForm};
export {Buttons, Button};