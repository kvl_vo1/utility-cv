import React from "react";
import PropTypes from "prop-types";

function ModalWrapper({onClick, children}) {
  return (
    <div className="msg-wrapper">
      <div className="msg-back-drop" onClick={onClick}/>
      <div className="msg-content">
        {children}
      </div>
    </div>
  );
}

ModalWrapper.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.any.isRequired
};

export default ModalWrapper;