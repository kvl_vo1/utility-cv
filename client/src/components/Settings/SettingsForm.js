import React, {useContext, useEffect, useState} from "react";
import PropTypes from "prop-types";
import produce from "immer";
import AppCtx from "../../context/AppCtx";
import {getBearer, httpClient, unixToLocaleTimestamp} from "../../common";
import {Button, Buttons} from "../Modal/Modal";
import {Column, Row} from "../Layout";

const TAB_GENERAL = "TAB_GENERAL";
const TAB_MAIL_SERVER = "TAB_MAIL_SERVER";

const FULL_NAME = "fullName";
const FLAT = "flat";
const IS_ADMIN = "isAdmin";
const EMAIL_RECIPIENT = "recipient";
const SERVER_HOST = "host";
const SERVER_LOGIN = "login";
const SERVER_PASSWORD = "password";

function SettingsForm({onCancel}) {
  const [tab, setTab] = useState(TAB_GENERAL);
  const [error, setError] = useState("");
  const [settings, setSettings] = useState(null);

  const ctx = useContext(AppCtx);
  const {state, setModal} = ctx;

  useEffect(() => {
    loadSettings();

    return function cleanUp() {
      // willUnmount
    }
  }, []);

  const onTabClick = e => {
    setTab(e.currentTarget.dataset.tab)
  };

  const onChangeField = e => {
    const {value, dataset: {field}} = e.target;

    setSettings((old) => {
      return produce(old, s => {
        switch (field) {
          case FULL_NAME:
            s.user[field] = value;
            break;

          case FLAT:
            s.user[field] = value;
            break;

          case IS_ADMIN:
            s.user[field] = !s.user[field];
            break;

          case EMAIL_RECIPIENT:
            s.email[field] = value;
            break;

          case SERVER_HOST:
            s.email.server[field] = value;
            break;

          case SERVER_LOGIN:
            s.email.server[field] = value;
            break;

          case SERVER_PASSWORD:
            s.email.server[field] = value;
            break;
        }
      });
    })
  };

  const onApply = () => {
    saveSettings(settings)
  };

  const loadSettings = () => {
    httpClient().get("/settings", {
      headers: {
        Authorization: getBearer(state.auth.token)
      }
    })
      .then(response => {
        const {data} = response;
        delete data.user["_id"];
        delete data.user["login"];
        delete data.user["maxConn"];
        delete data.user["periodId"];

        if (data.email) {
          delete data.email.server["name"];
        }

        setSettings(data);
      })
      .catch(error => {
        if (error.response) {
          const {data, status, statusText} = error.response;
          setError(data ? data : `${status}: ${statusText}`);
        } else {
          setError(error.message);
        }
      })
      .finally(() => {

      });
  };

  const saveSettings = data => {
    httpClient().post("/settings", JSON.stringify(data), {
      headers: {
        Authorization: getBearer(state.auth.token)
      }
    })
      .then(response => {
        setModal(null);
      })
      .catch(error => {
        if (error.response) {
          const {data, status, statusText} = error.response;
          setError(data ? data : `${status}: ${statusText}`);
        } else {
          setError(error.message);
        }
      })
      .finally(() => {

      });
  };

  if (error && !settings) {
    return (
      <Row>
        <Column className="is-12">
          <div className="notification pb-0">
            <p className="has-text-weight-bold has-text-danger">{error}</p>
          </div>
        </Column>
      </Row>
    )
  }

  if (!settings) {
    return (
      <Row>
        <Column className="is-12 pb-0">
          <div className="notification">
            <p className="has-text-weight-bold">Зачекайте, йде завантаження...</p>
          </div>
        </Column>
      </Row>
    )
  }

  const {user = null, email = null} = settings;

  return (
    <>
      <div className="message-body">
        <div className="columns">
          <div className="column is-12">
            {
              error && <Row>
                <Column className="is-12">
                  <div className="notification mb-2 is-danger">
                    <p className="has-text-weight-bold">{error}</p>
                  </div>
                </Column>
              </Row>
            }

            {
              settings && <div className="tabs is-boxed">
                <ul>
                  {
                    user &&
                    <li className={tab === TAB_GENERAL ? "is-active" : ""} data-tab={TAB_GENERAL} onClick={onTabClick}>
                      <a className="ch-link">
                <span className="icon is-small">
                  <i className="fal fa-user-cog"></i>
                </span>
                        <span>Загальні</span>
                      </a>
                    </li>
                  }

                  {
                    email &&
                    <li className={tab === TAB_MAIL_SERVER ? "is-active" : ""} data-tab={TAB_MAIL_SERVER}
                        onClick={onTabClick}>
                      <a className="ch-link">
                <span className="icon is-small">
                  <i className="fal fa-mailbox"></i>
                </span>
                        <span>Поштовий сервер</span>
                      </a>
                    </li>
                  }
                </ul>
              </div>
            }

            {
              tab === TAB_GENERAL && settings && user &&
              <div className="is-block tab-general">
                <div className="field">
                  <label className="label">Ім&#39;я власника:</label>
                  <div className="control has-icons-left">
                    <input className="input"
                           type="text"
                           placeholder="П.І.П."
                           onChange={onChangeField}
                           data-field={FULL_NAME}
                           value={user.fullName}
                    />
                    <span className="icon is-small is-left">
                  <i className="fal fa-text"></i>
                </span>
                  </div>
                </div>

                <div className="field">
                  <label className="label">Квартира:</label>
                  <div className="control has-icons-left">
                    <input className="input"
                           type="text"
                           placeholder="Номер квартири"
                           onChange={onChangeField}
                           data-field={FLAT}
                           value={user.flat}
                    />
                    <span className="icon is-small is-left">
                  <i className="fal fa-text"></i>
                </span>
                  </div>
                </div>

                <div className="field">
                  <label className="checkbox">
                    <input
                      type="checkbox"
                      disabled={true /*!cbIsAdmin.current*/}
                      checked={user.isAdmin}
                      onChange={onChangeField}
                      data-field={IS_ADMIN}
                    />
                    &nbsp;<span>Адміністратор</span>
                  </label>
                </div>

                <div className="field">
                  <label className="label">Дата створення акаунту:</label>
                  <div className="control has-icons-left">
                    <input className="input"
                           type="text"
                           placeholder="Text input"
                           value={unixToLocaleTimestamp(user.createdAt * 1000)}
                           disabled
                    />
                    <span className="icon is-small is-left">
                  <i className="fal fa-calendar-alt"></i>
                </span>
                  </div>
                </div>
              </div>
            }

            {
              tab === TAB_MAIL_SERVER && settings && email &&
              <div className="is-block tab-mail-server">
                <div className="field">
                  <label className="label">Електропошта отримувача:</label>
                  <div className="control has-icons-left">
                    <input className="input"
                           type="email"
                           placeholder="mail@to-target.recipient"
                           value={email.recipient}
                           onChange={onChangeField}
                           data-field={EMAIL_RECIPIENT}
                    />
                    <span className="icon is-small is-left">
                  <i className="fal fa-user"></i>
                </span>
                  </div>
                </div>

                <div className="field">
                  <label className="label">Поштовий сервер:</label>
                  <div className="control has-icons-left">
                    <input className="input"
                           type="email"
                           placeholder="server.host.name:port"
                           value={email.server.host}
                           onChange={onChangeField}
                           data-field={SERVER_HOST}
                    />
                    <span className="icon is-small is-left">
                  <i className="fal fa-mailbox"></i>
                </span>
                  </div>
                </div>

                <div className="field">
                  <label className="label">Логін:</label>
                  <div className="control has-icons-left">
                    <input className="input"
                           type="email"
                           placeholder="mail@server.account"
                           value={email.server.login}
                           onChange={onChangeField}
                           data-field={SERVER_LOGIN}
                    />
                    <span className="icon is-small is-left">
                  <i className="fal fa-user"></i>
                </span>
                  </div>
                </div>

                <div className="field">
                  <label className="label">Пароль:</label>
                  <div className="control has-icons-left">
                    <input className="input"
                           type="password"
                           autoComplete="off"
                           placeholder="Service password"
                           value={email.server.password || ""}
                           onChange={onChangeField}
                           data-field={SERVER_PASSWORD}
                    />
                    <span className="icon is-small is-left">
                  <i className="fal fa-lock"></i>
                </span>
                  </div>
                </div>
              </div>
            }
          </div>
        </div>
      </div>

      <Buttons>
        <Button type="is-danger" onClick={onCancel} caption="Скасувати"/>
        <Button type="is-primary" onClick={onApply} caption="Зберегти і вийти"/>
      </Buttons>
    </>
  );
}

SettingsForm.propTypes = {
  onCancel: PropTypes.func.isRequired,
};

export default SettingsForm;