import React, {useContext} from "react";
import PropTypes from "prop-types";
import {Column, Level, Row} from "../index";
import AppCtx from "../../context/AppCtx";
import {ELECTRICITY, INTERCOM, INTERNET, PHONE, WATER} from "../../common";
import {msgConfirm} from "../Modal/Modal";

function RecipeImages({type, receiptId, periodId, images}) {
  const ctx = useContext(AppCtx);
  const {setInProgress, sendFiles, removeFile} = ctx;

  const onAddFile = e => {
    e.preventDefault();

    const {files} = e.target;

    const formData = new FormData();
    formData.append("type", type);
    formData.append("periodId", periodId);

    for (let i = 0; i < files.length; i++) {
      formData.append("files", files[i])
    }

    e.target.files = null;
    e.target.value = "";

    setInProgress(true);
    sendFiles(formData)
  };

  const onDelete = e => {
    e.stopPropagation();
    const {index} = e.currentTarget.dataset;

    const onConfirm = () => {
      const data = {
        receiptId,
        receiptSubType: type,
        fileId: images[index]._id
      };

      setInProgress(true);
      removeFile(data)
    };

    msgConfirm(ctx, <p>Видалити файл?</p>, "Увага!", onConfirm);
  };

  const onOpenItem = e => {
    e.stopPropagation();
    const {index} = e.currentTarget.dataset;
    window.open(images[index].originalPath);
  };

  return (
    <Row>
      <Column className="is-12">
        <Level className="receipt-images is-flex">
          {
            images && images.map((image, index) => {
              return (
                <div key={image._id} data-index={index} onClick={onOpenItem} className="card">
                  <div className="card-image">
                    <figure className="image is-64x64">
                      {
                        image.contentType.startsWith("image/")
                          ? <img className="ch-align-center" src={image.thumbPath} alt={image.thumbPath}/>
                          : <i className="fal fa-file-pdf fa-2x"></i>
                      }
                    </figure>
                  </div>
                  <div className="delete-img" data-index={index} onClick={onDelete}>
                    <i className="fas fa-times-circle fa-2x"></i>
                  </div>
                </div>
              )
            })
          }
          <div className="card">
            <div className="card-image">
              <figure className="image is-64x64">
                <i className="fal fa-plus fa-2x"></i>
                <input className="file-input"
                       type="file"
                       name="files"
                       multiple={false}
                       accept=".jpg, .jpeg, .png, .pdf"
                       onChange={onAddFile}
                />
              </figure>
            </div>
          </div>
        </Level>
      </Column>
    </Row>
  );
}

RecipeImages.propTypes = {
  type: PropTypes.oneOf([INTERNET, WATER, INTERCOM, ELECTRICITY, PHONE]).isRequired,
  receiptId: PropTypes.string.isRequired,
  periodId: PropTypes.number.isRequired,
  images: PropTypes.array.isRequired
};

export default RecipeImages;