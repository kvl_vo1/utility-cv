import React, {useContext, useState} from "react";
import PropTypes from "prop-types";
import {ELECTRICITY, HEATING, INTERCOM, INTERNET, PHONE, WATER, getReceiptMeta} from "../../common";
import ReceiptField from "./ReceiptField";
import {Column, Panel, Row} from "../index";
import ReceiptTable from "./ReceiptTable";
import AppCtx from "../../context/AppCtx";
import RecipeImages from "./RecipeImages";

function Receipt({type, locked, initial}) {
  const [isExpanded, setExpanded] = useState(!locked);
  const {state: {receipt}} = useContext(AppCtx);
  let {icon, root, name, fields} = getReceiptMeta(type);

  const images = receipt && receipt[root] && receipt[root].files
    ? receipt[root].files
    : [];

  const periodId = receipt && receipt[root] && receipt.periodId
    ? receipt.periodId
    : 0;

  fields = fields.map(meta => {
    return {...meta, field: `${root}/${meta.field}`, type};
  });

  const onToggle = () => {
    setExpanded(oldState => !oldState);
  };

  return (
    <Column className="is-12">
      <Panel className={`receipt ${locked ? "is-light" : "is-warning"}`}>
        {/*Header*/}
        <p className={`panel-heading has-text-centered ${isExpanded ? "" : "ch-round"}`}>
          <span className="icon ch-sticky-left">
            <i className={`fal ${icon}`}></i>
          </span>
          <span className="icon ch-sticky-right ch-cursor-pointer" onClick={onToggle}>
            <i className={`fal ${isExpanded ? "fa-minus" : "fa-plus"}`}></i>
          </span>
          {locked && <i className="fal fa-lock mr-3"></i>}
          {name}
        </p>

        {/*Fields*/}
        {
          isExpanded && <div className="panel-block is-block">
            <Row>
              <Column className="is-12">
                {
                  type === WATER
                    ? <ReceiptTable meta={fields} initial={initial} locked={locked}/>
                    : fields.map((meta, key) => <ReceiptField key={key} meta={meta} locked={locked}/>)
                }
              </Column>
            </Row>

            {/*Images*/}
            {type !== HEATING && <RecipeImages receiptId={receipt._id} type={type} periodId={periodId} images={images}/>}
          </div>
        }
      </Panel>
    </Column>
  );
}

Receipt.propTypes = {
  initial: PropTypes.bool.isRequired,
  locked: PropTypes.bool.isRequired,
  type: PropTypes.oneOf([INTERNET, WATER, INTERCOM, ELECTRICITY, HEATING, PHONE]).isRequired
};

export default Receipt;