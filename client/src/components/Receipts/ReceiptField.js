import React, {useContext, useEffect, useState} from "react";
import PropTypes from "prop-types";
import {checkFieldTypeAndRange} from "../../common";
import AppCtx from "../../context/AppCtx";

function ReceiptField({meta, locked}) {
  const [field, setField] = useState(() => {
    return {
      ...meta,
      touched: false,
      value: 0,
      prevValue: 0,
      error: null
    }
  });

  const {state, updateReceipt} = useContext(AppCtx);

  useEffect(() => {
    const fields = field.field.split("/");

    let v = state.receipt;
    for (let fld of fields) {
      v = v[fld];
    }

    let value = 0;
    let prevValue = null;
    if (Array.isArray(v)) {
      value = v[0];
      prevValue = v[1];
    } else {
      value = v
    }

    setField(old => {
      return {
        ...old,
        value,
        prevValue
      }
    });
  }, []);

  const onChange = e => {
    e.persist();

    setField(old => {
      return {...old, value: e.target.value}
    })
  };

  const onBlur = () => {
    let {val, error} = checkFieldTypeAndRange(field.fieldType, field.value, field.prevValue);

    if (!error) {
      error = field.value < field.prevValue ? `Допускаються значення >= ${field.prevValue}` : null;
    }

    setField(old => {
      return {
        ...old,
        touched: true,
        value: val,
        error
      }
    });

    if (!error) {
      updateReceipt(field.field, val)
    }
  };

  const hasError = field.touched && field.error;
  const label = meta.label + (field.prevValue
    ? ` (попередній:${field.prevValue})`
    : "");

  return (
    <div className="field">
      <label className="label">{label}</label>
      <div className="control has-icons-left">
        <input
          //id={field.field}
          //data-receipt={field.type}
          //data-type={field.fieldType}
          disabled={locked}
          className="input"
          type="text"
          placeholder={field.placeholder}
          onChange={locked ? null : onChange}
          onBlur={locked ? null : onBlur}
          value={field.value}
        />
        <span className="icon is-small is-left">
          <i className="fal fa-abacus"></i>
        </span>
      </div>
      <p className={`help is-danger ${hasError ? "" : "is-hidden"}`}>{field.error}</p>
    </div>
  );
}

ReceiptField.propTypes = {
  meta: PropTypes.object.isRequired,
  locked: PropTypes.bool.isRequired
};

export default ReceiptField;