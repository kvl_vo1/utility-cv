import React, {useContext, useEffect, useState} from "react";
import PropTypes from "prop-types";
import Receipt from "./Receipt";
import {getMonthMeta, getReceiptTypes} from "../../common";
import {Column, Progress, Row, Level, LevelSide, LevelItem} from "../index";
import AppCtx from "../../context/AppCtx";

function Receipts(props) {
  const {state, setInProgress, setReceipt, saveReceipt} = useContext(AppCtx);
  const [periodId, setPeriodId] = useState(0);

  const isLocked = state.receipt && state.receipt.isLocked;
  const isInitial = state.receipt && state.receipt.isInitial;

  useEffect(() => {
    setPeriodId(props.match.params.periodId);
  }, []);

  useEffect(() => {
    setReceipt(periodId);
  }, [periodId]);

  if (state.receipts.length === 0) {
    props.history.replace("/");
  }

  const goToHome = () => {
    props.history.replace("/");
  };

  const saveChanges = () => {
    setInProgress(true);
    saveReceipt();
  };

  const makeMonth = period => {
    const p = String(period);

    if (p.length === 6) {
      const year = Number(p.substring(0, 4));
      const month = Number(p.substring(4));

      return `${getMonthMeta(month).name} ${year}`
    } else {
      return ""
    }
  };

  return (
    <Row className="is-block">
      <Column className="is-6 is-offset-3">
        <Column className="is-12">
          <div className={`notification py-3 px-3 is-info has-text-centered ${isLocked ? "is-light" : ""}`}>
            <p className="is-size-3">
              {isLocked && <i className="fal fa-lock mr-3"></i>}
              {makeMonth(periodId)}
            </p>
          </div>
        </Column>

        {
          state.common.error && <Column className="is-12">
            <div className="notification is-danger">
              {state.common.error}
            </div>
          </Column>
        }

        {state.common.inProgress && <Progress/>}

        {
          state.receipt
            ? getReceiptTypes().map((type, key) => <Receipt
              key={key}
              type={type}
              locked={isLocked}
              initial={isInitial}
              {...props}
            />)
            : "'state.receipt' is empty"
        }

        <Column className="is-12">
          <div className="notification py-3 px-3">
            <Level className="is-flex">
              <LevelSide className="level-left">
                <LevelItem>
                  <button className="button is-info px-3" onClick={goToHome}>
                    <span className="icon is-small">
                      <i className="fal fa-home"></i>
                    </span>
                    <span>На головну</span>
                  </button>
                </LevelItem>
              </LevelSide>
              {
                !isLocked &&
                <LevelSide className="level-right">
                  <LevelItem>
                    <button className="button is-primary px-3" onClick={saveChanges}>
                      <span className="icon is-small">
                        <i className="fal fa-check-circle"></i>
                      </span>
                      <span>Зберегти зміни</span>
                    </button>
                  </LevelItem>
                </LevelSide>
              }
            </Level>
          </div>
        </Column>
      </Column>
    </Row>
  );
}

Receipts.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default Receipts;