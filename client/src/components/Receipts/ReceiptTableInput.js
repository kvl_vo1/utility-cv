import React from "react";
import PropTypes from "prop-types";

function ReceiptTableInput({name, index, value, editable, change, blur}) {
  return editable
    ? <input
      data-field={name}
      data-index={index}
      className={value.error ? "has-background-danger has-text-white" : "has-background-warning-light"}
      type="text"
      value={value.value}
      onChange={change}
      onBlur={blur}
    />
    : value.value
}

ReceiptTableInput.propTypes = {
  name: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  value: PropTypes.object.isRequired,
  editable: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  blur: PropTypes.func.isRequired,
};

export default ReceiptTableInput;