import React, {useContext, useEffect, useRef, useState} from "react";
import PropTypes from "prop-types";
import produce from "immer";
import AppCtx from "../../context/AppCtx";
import {checkFieldTypeAndRange, isValidInteger, unixToLocaleTimestamp} from "../../common";
import ReceiptTableInput from "./ReceiptTableInput";
import {msgConfirm, msgError} from "../Modal/Modal";
import {Level, LevelItem, LevelSide} from "../index";

function ReceiptTable({meta, initial, locked}) {
  const makeFieldsMeta = meta => {
    let res = {};

    meta.map(item => {
      res[item.field] = {
        ...item,
        touched: false,
        value: 0,
        error: null,
        prevTouched: false,
        prevValue: 0,
        prevError: null,
        total: ""
      };

      delete res[item.field].label;
      delete res[item.field].placeholder;
      delete res[item.field].type;
      delete res[item.field].field;
    });

    return res;
  };

  const [fields, setFields] = useState(() => makeFieldsMeta(meta));
  const [rcpField, setRcpField] = useState({name: "", values: [0, 0]});
  const [isSent, setIsSent] = useState({isOk: false});

  const isInit = useRef(false);

  const ctx = useContext(AppCtx);
  const {state, updateReceipt, setInProgress, saveReceipt, sendReceipt, loadReceipt} = ctx;

  useEffect(() => {
    if (rcpField.name) {
      updateReceipt(rcpField.name, rcpField.values);
    }
  }, [rcpField]);

  useEffect(() => {
    if (!isInit.current) return;

    if (!isSent.isOk) {
      msgError(ctx, "Деякі поля заповнені невірно!", "Помилка!")
    } else {
      const msg = <p>Показники по воді будуть відправлені на електронну адресу.<br/>Продовжити?</p>;

      const onConfirm = () => {
        const sr = periodId => {
          return () => sendReceipt(periodId, () => {
            // setInProgress(true);
            loadReceipt(periodId)
          })
        };

        setInProgress(true);

        if (!locked) {
          saveReceipt(sr(state.receipt.periodId))
        } else {
          sr(state.receipt.periodId)()
        }
      };

      msgConfirm(ctx, msg, "Увага!", onConfirm);
    }
  }, [isSent]);

  useEffect(() => {
    for (let key in fields) {
      const list = key.split("/");

      let v = state.receipt;
      for (let fld of list) {
        v = v[fld];
      }

      setFields(old => {
        return produce(old, draft => {
          draft[key].value = v[0];
          draft[key].prevValue = v[1];
        })
      })
    }

    isInit.current = true;
  }, []);

  const getValue = (fld, index) => {
    switch (index) {
      case 0:
        return {
          value: fields[fld].value,
          error: fields[fld].error,
        };

      case 1:
        return {
          value: fields[fld].prevValue,
          error: fields[fld].prevError,
        };

      default:
        return {
          value: 0,
          error: `wrong index: ${index}`,
        }
    }
  };

  const getTotalValue = fld => {
    if (!fields[fld].error && !fields[fld].prevError) {
      const v1 = isValidInteger(fields[fld].value) ? parseInt(fields[fld].value) : 0;
      const v2 = isValidInteger(fields[fld].prevValue) ? parseInt(fields[fld].prevValue) : 0;

      return v1 - v2
    } else {
      return 0
    }
  };

  const getTotal = (root, bathroom, kitchen) => {
    const b = `${root}/${bathroom}`;
    const k = `${root}/${kitchen}`;

    return getTotalValue(b) + getTotalValue(k)
  };

  const onChange = e => {
    e.persist();

    let {value, dataset: {index, field}} = e.target;
    index = Number(index);

    setFields(old => {
      return produce(old, draft => {
        if (index === 0) {
          // current value
          draft[field].value = value
        } else if (index === 1) {
          // previous value
          draft[field].prevValue = value
        }

        return draft;
      });
    });
  };

  const onBlur = e => {
    e.persist();

    const {dataset: {field: name}} = e.target;

    // перевірка правильності введених значень (синтексис і діапазон)
    setFields(old => {
      return produce(old, draft => {
        {
          const {val, error} = checkFieldTypeAndRange(draft[name].fieldType, draft[name].value);
          draft[name].touched = true;
          draft[name].value = val;
          draft[name].error = error;
        }

        {
          const {val, error} = checkFieldTypeAndRange(draft[name].fieldType, draft[name].prevValue);
          draft[name].prevTouched = true;
          draft[name].prevValue = val;
          draft[name].prevError = error;
        }

        if (!draft[name].error && !draft[name].prevError) {
          if (draft[name].value < draft[name].prevValue) {
            draft[name].error = "Поточне значення повинно бути >= за попереднє.";
            draft[name].prevError = "Попереднє значення повинно бути <= за поточне.";
          }
        }

        return draft;
      })
    });

    setFields(old => {
      let v;
      const list = name.split("/");

      let rc = state.receipt;
      for (let fld of list) {
        rc = rc[fld];
      }

      v = [...rc];

      if (!old[name].error) {
        v[0] = old[name].value
      }

      if (!old[name].prevError) {
        v[1] = old[name].prevValue
      }

      setRcpField(() => {
        return {name, values: v};
      });

      return old;
    });
  };

  const onSend = () => {
    setFields(old => {
      let isOk = true;

      for (let fld in old) {
        isOk = (old[fld].error === null) && (old[fld].prevError === null);
        if (!isOk) break;
      }

      setIsSent({isOk});

      return old;
    });
  };

  return (
    <>
      <table className="table is-bordered is-narrow is-fullwidth mb-2">
        <tbody>
        <tr>
          <td colSpan="5" rowSpan="2">Споживання води за показниками лічильників</td>
          <td colSpan="3" className="has-text-centered">Показники, куб. м</td>
        </tr>
        <tr>
          <td>Поточні</td>
          <td>Попередні</td>
          <td className="has-text-weight-bold">Спожито</td>
        </tr>
        <tr>
          <td colSpan="2" rowSpan="3">Холодна вода</td>
          <td colSpan="3">1-й ліч (санвузол)</td>
          <td>
            <ReceiptTableInput name="utility/coldWater/bathroom"
                               index={0}
                               value={getValue("utility/coldWater/bathroom", 0)}
                               editable={!locked}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td>
            <ReceiptTableInput name="utility/coldWater/bathroom"
                               index={1}
                               value={getValue("utility/coldWater/bathroom", 1)}
                               editable={!locked && initial}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td className="has-text-weight-bold">{getTotalValue("utility/coldWater/bathroom")}</td>
        </tr>
        <tr>
          <td colSpan="3">2-й ліч (кухня)</td>
          <td>
            <ReceiptTableInput name="utility/coldWater/kitchen"
                               index={0}
                               value={getValue("utility/coldWater/kitchen", 0)}
                               editable={!locked}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td>
            <ReceiptTableInput name="utility/coldWater/kitchen"
                               index={1}
                               value={getValue("utility/coldWater/kitchen", 1)}
                               editable={!locked && initial}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td className="has-text-weight-bold">{getTotalValue("utility/coldWater/kitchen")}</td>
        </tr>
        <tr>
          <td colSpan="3">Разом</td>
          <td colSpan="3"
              className={`has-text-weight-bold has-text-right ${locked ? "" : "has-background-warning-light"}`}>
            {getTotal("utility/coldWater", "bathroom", "kitchen")}
          </td>
        </tr>
        <tr>
          <td colSpan="2" rowSpan="3">Гаряча вода</td>
          <td colSpan="3">1-й ліч (санвузол)</td>
          <td>
            <ReceiptTableInput name="utility/hotWater/bathroom"
                               index={0}
                               value={getValue("utility/hotWater/bathroom", 0)}
                               editable={!locked}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td>
            <ReceiptTableInput name="utility/hotWater/bathroom"
                               index={1}
                               value={getValue("utility/hotWater/bathroom", 1)}
                               editable={!locked && initial}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td className="has-text-weight-bold">{getTotalValue("utility/hotWater/bathroom")}</td>
        </tr>
        <tr>
          <td colSpan="3">2-й ліч (кухня)</td>
          <td>
            <ReceiptTableInput name="utility/hotWater/kitchen"
                               index={0}
                               value={getValue("utility/hotWater/kitchen", 0)}
                               editable={!locked}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td>
            <ReceiptTableInput name="utility/hotWater/kitchen"
                               index={1}
                               value={getValue("utility/hotWater/kitchen", 1)}
                               editable={!locked && initial}
                               change={onChange}
                               blur={onBlur}/>
          </td>
          <td className="has-text-weight-bold">{getTotalValue("utility/hotWater/kitchen")}</td>
        </tr>
        <tr>
          <td colSpan="3">Разом</td>
          <td colSpan="3"
              className={`has-text-weight-bold has-text-right ${locked ? "" : "has-background-warning-light"}`}>
            {getTotal("utility/hotWater", "bathroom", "kitchen")}
          </td>
        </tr>
        </tbody>
      </table>

      <Level className="is-flex mb-1">
        <LevelSide className="level-left"/>
        <LevelSide className="level-right">
          <LevelItem>
            <button className="button is-info" onClick={onSend}>
              <span className="icon is-small">
                <i className="fal fa-envelope"></i>
              </span>
              <span>Відправити лист з показниками</span>
            </button>
          </LevelItem>
        </LevelSide>
      </Level>

      {
        state.receipt &&
        <div className="level is-flex">
          <div className="level-left"/>
          <div className="level-right">
            <p>{
              state.receipt.utility.sentAt > 0
                ? <>Востаннє відправлено:&nbsp;
                  <span className="has-text-success has-text-weight-bold">{unixToLocaleTimestamp(state.receipt.utility.sentAt * 1000)}</span>
                </>
                : <span className="has-text-danger has-text-weight-bold">Показники жодного разу не відправлялися</span>
            }
            </p>
          </div>
        </div>
      }
    </>
  );
}

ReceiptTable.propTypes = {
  meta: PropTypes.array.isRequired,
  initial: PropTypes.bool.isRequired,
  locked: PropTypes.bool.isRequired,
};

export default ReceiptTable;