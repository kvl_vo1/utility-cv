import React, {useContext, useEffect} from "react";
import Wrapper from "./Wrapper";
import ItemMonth from "./ItemMonth";
import AppCtx from "../../context/AppCtx";

function Dashboard(props) {
  const ctx = useContext(AppCtx);
  const {state, setInProgress, setReceipts} = ctx;

  useEffect(() => {
    setInProgress(true);
    setReceipts();
  }, []);

  return (
    <Wrapper error={state.common.error} inProgress={state.common.inProgress} {...props}>
      <ItemMonth isNew={true} {...props}/>
      {
        state.receipts.map((item, id) => <ItemMonth key={id} receipt={item} {...props}/>)
      }
    </Wrapper>
  );
}

export default Dashboard;