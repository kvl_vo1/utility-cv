import React from "react";
import PropTypes from "prop-types";
import {Column, MainMenu, Panel, Progress, Row} from "../index";

function Wrapper({children, error, inProgress, ...rest}) {
  return (
    <Row className="pt-3">
      <Column className="is-6 is-offset-3">
        {
          error && <Row>
            <Column className="is-12">
              <div className="notification mb-2 is-danger">
                {error}
              </div>
            </Column>
          </Row>
        }

        <Row>
          <Column className="is-12">
            <MainMenu {...rest}/>
            <Panel className="is-relative">
              {inProgress && <Progress/>}
              <Panel className="is-flex month">
                {children}
              </Panel>
            </Panel>
          </Column>
        </Row>
      </Column>
    </Row>
  );
}

Wrapper.defaultProps = {
  error: "",
  inProgress: false,
};

Wrapper.propTypes = {
  children: PropTypes.any.isRequired,
  error: PropTypes.string.isRequired,
  inProgress: PropTypes.bool.isRequired,
};

export default Wrapper;