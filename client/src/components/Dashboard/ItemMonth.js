import React, {useContext} from "react";
import {useHistory} from "react-router-dom";
import PropTypes from "prop-types";
import {getMonthMeta, getReceiptMeta, getReceiptTypes, WATER} from "../../common";
import ItemWrapper from "./ItemWrapper";
import AppCtx from "../../context/AppCtx";
import {msgConfirm} from "../Modal/Modal";

function ItemMonth({isNew, receipt}) {
  const ctx = useContext(AppCtx);
  const {state, setInProgress, addNewPeriod} = ctx;
  const history = useHistory();

  const createMonthMeta = periodId => {
    const year = Number(String(periodId).substring(0, 4));
    const month = Number(String(periodId).substring(4));

    return {
      meta: getMonthMeta(month),
      year
    };
  };

  let monthMeta = null;

  const onConfirm = () => {
    setInProgress(true);
    addNewPeriod(history);
  };

  const getReceiptsList = () => {
    return getReceiptTypes().map((item, key) => {
      const meta = getReceiptMeta(item);

      const getData = (name, firstEl = false) => {
        const list = name.split("/");

        let rc = receipt;
        for (let fld of list) {
          rc = rc[fld];
        }

        if (firstEl && Array.isArray(rc) && rc.length > 0) {
          return rc[0]
        }

        return rc
      };

      const getInfo = type => {
        let res = meta.getTotalFieldPath();

        if (type === WATER) {
          res = JSON.parse(res);
          let info = [];

          for (let f in res) {
            const water = f.split("/")[1];

            let css = "";
            switch (water) {
              case "coldWater":
                css = "has-text-link";
                break;

              case "hotWater":
                css = "has-text-danger";
                break;

              default:
                css = "";
            }

            let sum = 0;
            res[f].map(sf => {
              const ff = `${f}/${sf}`;
              let x = getData(ff);

              sum += x[0] - x[1];
            });

            info = [...info, <span key={water} className={`${css} mr-1`}>{sum}м<sup>3</sup></span>]
          }

          res = info
        } else {
          res = `${getData(res)} грн.`
        }

        return <span className="has-text-weight-bold">{res}</span>
      };

      return (
        <p key={key}>
          <span className="icon mr-3">
            <i className={`fal ${meta.icon}`}></i>
          </span>
          <span className="mr-3">{meta.name}:</span>
          {getInfo(item)}
        </p>
      )
    })
  };

  const onClick = () => {
    if (isNew) {
      let msg = "";

      if (state.receipts.length > 0) {
        monthMeta = createMonthMeta(state.receipts[0].periodId);

        msg = <p>
          Поточний місяць <strong>({monthMeta.meta.name} {monthMeta.year})</strong> буде заблоковано.
          <br/>
          В тебе не буде можливості внести зміни.
          <br/>
          Продовжити?
        </p>
      } else {
        msg = <p>Впевнений що хочеш створити <strong>ініціалізаційний</strong> місяць?</p>
      }

      msgConfirm(ctx, msg, "Увага!", onConfirm)
    } else {
      history.push(`/month/${receipt.periodId}`)
    }
  };

  if (receipt) {
    monthMeta = createMonthMeta(receipt.periodId)
  }

  return (
    <ItemWrapper isNew={isNew} onClick={onClick}>
      {
        isNew
          ? <span className="icon is-large ch-align-center">
              <i className="fal fa-plus fa-6x"></i>
            </span>
          : <>
            <figure className="month-img-wrapper">
              <img src={monthMeta.meta.image} alt="image"/>
            </figure>
            <div className="month-content-frame">
              <p className="ch-stroke">{`${monthMeta.meta.name} ${monthMeta.year}`}</p>
            </div>
          </>
      }
      {
        !isNew && <div className="month-info">
          <div className="back-drop"/>
          <div className="month-content-frame">
            {getReceiptsList()}
          </div>
        </div>
      }
    </ItemWrapper>
  );
}

ItemMonth.defaultProps = {
  isNew: false,
  receipt: null
};

ItemMonth.propTypes = {
  isNew: PropTypes.bool.isRequired,
  receipt: PropTypes.object,
};

export default ItemMonth;