import PropTypes from "prop-types";
import React from "react";
import classNames from "classnames";

function ItemWrapper({children, isNew, onClick}) {
  const cl = classNames("month-wrapper ch-cursor-pointer hovering", {"new-period": isNew});

  return <div className={cl} onClick={onClick}>
    <div className="month-content">
      {children}
    </div>
  </div>
}

ItemWrapper.defaultProps = {
  isNew: false,
};

ItemWrapper.propTypes = {
  children: PropTypes.any.isRequired,
  isNew: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default  ItemWrapper;

