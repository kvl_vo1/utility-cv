package controllers

import (
  "crypto/aes"
  "crypto/cipher"
  "encoding/hex"
)

type Crypter struct {
  key string
  iv  []byte
}

func NewCrypter() *Crypter {
  c := new(Crypter)

  b, _ := hex.DecodeString("5e7172712a23474436353876316f736f744c216a21312536365a77215348786e")
  c.key = string(b)

  c.iv, _ = hex.DecodeString("3134cc790e3ed8d2bbf0c55e")

  return c
}

func (c *Crypter) Encrypt(text string) (string, error) {
  block, err := aes.NewCipher([]byte(c.key))
  if err != nil {
    return "", err
  }

  aesgcm, err := cipher.NewGCM(block)
  if err != nil {
    return "", err
  }

  ciphertext := aesgcm.Seal(nil, c.iv, []byte(text), nil)
  return hex.EncodeToString(ciphertext), nil
}

func (c *Crypter) Decrypt(ct string) (string, error) {
  ciphertext, _ := hex.DecodeString(ct)

  block, err := aes.NewCipher([]byte(c.key))
  if err != nil {
    return "", err
  }

  aesgcm, err := cipher.NewGCM(block)
  if err != nil {
    return "", err
  }

  plaintext, err := aesgcm.Open(nil, c.iv, ciphertext, nil)
  if err != nil {
    return "", err
  }

  s := string(plaintext[:])
  return s, nil
}
