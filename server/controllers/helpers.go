package controllers

import (
  "crypto/sha1"
  "crypto/tls"
  "encoding/base64"
  "encoding/hex"
  "fmt"
  "io"
  "math/rand"
  "net"
  "net/mail"
  "net/smtp"
  "regexp"
  "strconv"
  "strings"
  "time"

  "utility/entity"
)

const (
  MinPeriodId = int64(202001)
  MaxPeriodId = int64(202212)
)

func GetHash(s string) (string, error) {
  h := sha1.New()
  _, err := io.WriteString(h, s)
  if err != nil {
    return "", err
  }
  return hex.EncodeToString(h.Sum(nil)), nil
}

func ComparePasswordByHash(password, hash string) (bool, error) {
  ph, err := GetHash(password)
  if err != nil {
    return false, err
  }

  return ph == hash, nil
}

func calcPeriodId(periodId int64, isNext bool) (int64, error) {
  if IsPeriodRangeOk(periodId) {
    y, _ := strconv.Atoi(strconv.Itoa(int(periodId))[:4])
    m, _ := strconv.Atoi(strconv.Itoa(int(periodId))[4:])

    if isNext {
      m++
      if m > 12 {
        m = 1
        y++
      }
    } else {
      m--
      if m < 1 {
        m = 12
        y--
      }
    }

    periodId = int64(y*100 + m)

    if IsPeriodRangeOk(periodId) {
      return periodId, nil
    }
  }

  return 0, fmt.Errorf("unsupported period \"%d\"", periodId)
}

func IsPeriodRangeOk(periodId int64) bool {
  return periodId >= MinPeriodId && periodId <= MaxPeriodId
}

func NextPeriodId(periodId int64) (int64, error) {
  return calcPeriodId(periodId, true)
}

func PrevPeriodId(periodId int64) (int64, error) {
  return calcPeriodId(periodId, false)
}

func MakeRandomPassword(n int) string {
  minChar := 33
  maxChar := 126
  p := make([]rune, n)

  rand.Seed(time.Now().UnixNano())

  for i := range p {
    p[i] = rune(minChar + rand.Intn(maxChar-minChar+1))
  }

  return string(p)
}

func SendEmail(server entity.EMail, subject, body string, to []string) error {
  // make ["..@..", "..@..", ...] to "<..@..>, <..@..>, ..."
  makeList := func(to []string) string {
    res := make([]string, 0, 10)

    for _, val := range to {
      res = append(res, fmt.Sprintf("<%s>", strings.TrimSpace(val)))
    }

    return strings.Join(res, ", ")
  }

  toList, err := mail.ParseAddressList(makeList(to))
  if err != nil {
    return err
  }

  to = make([]string, 0, 10)
  for _, a := range toList {
    to = append(to, a.Address)
  }

  // Setup headers
  headers := make(map[string]string, 10)
  headers["From"] = server.Server.Login
  headers["To"] = strings.Join(to, ", ")
  headers["Subject"] = subject
  headers["MIME-version"] = "1.0"
  headers["Content-Type"] = "text/html; charset=utf-8"
  headers["Content-Transfer-Encoding"] = "base64"

  // Setup message
  message := ""
  for k, v := range headers {
    message += fmt.Sprintf("%s: %s\r\n", k, v)
  }
  message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(body)) + "\r\n"

  host, _, _ := net.SplitHostPort(server.Server.Host)
  auth := smtp.PlainAuth("", server.Server.Login, server.Server.Password, host)

  // TLS config
  tlsconfig := &tls.Config{
    ServerName:         host,
    InsecureSkipVerify: true,
  }

  // Here is the key, you need to call tls.Dial instead of smtp.Dial
  // for smtp servers running on 465 that require an ssl connection
  // from the very beginning (no starttls)
  conn, err := tls.Dial("tcp", server.Server.Host, tlsconfig)
  if err != nil {
    return err
  }

  c, err := smtp.NewClient(conn, host)
  if err != nil {
    return err
  }

  // Auth
  if err = c.Auth(auth); err != nil {
    return err
  }

  // To && From
  if err = c.Mail(headers["From"]); err != nil {
    return err
  }

  for _, a := range to {
    //c.Rcpt(headers["To"])
    if err = c.Rcpt(a); err != nil {
      return err
    }
  }

  // Data
  w, err := c.Data()
  if err != nil {
    return err
  }

  _, err = w.Write([]byte(message))
  if err != nil {
    return err
  }

  err = w.Close()
  if err != nil {
    return err
  }

  err = c.Quit()
  if err != nil {
    return err
  }

  return nil
}

func IsEmailCorrect(email string) error {
  re := regexp.MustCompile(`(?mis)^([a-zA-Z0-9_.+-]+)@([a-zA-Z\d.-]+\.)+([a-zA-Z]{2,})$`)
  if !re.MatchString(email) {
    return ErrBadEmail
  }
  return nil
}

func ParsePeriodId(periodId int64) []string {
  y, _ := strconv.Atoi(strconv.Itoa(int(periodId))[:4])
  m, _ := strconv.Atoi(strconv.Itoa(int(periodId))[4:])
  month := ""

  switch m {
  case 1:
    month = "Січень"
  case 2:
    month = "Лютий"
  case 3:
    month = "Березень"
  case 4:
    month = "Квітень"
  case 5:
    month = "Травень"
  case 6:
    month = "Червень"
  case 7:
    month = "Липень"
  case 8:
    month = "Серпень"
  case 9:
    month = "Вересень"
  case 10:
    month = "Жовтень"
  case 11:
    month = "Листопад"
  case 12:
    month = "Грудень"
  default:
    month = fmt.Sprintf("%d - невірний місяць", m)
  }

  return []string{month, strconv.Itoa(int(y))}
}
