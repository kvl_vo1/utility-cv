package controllers

import "strings"

type ErrStacker interface {
  Add(e error, msg string)
  HasErrors() bool
  error
}

type errItem struct {
  e   error
  msg string
}

type ErrStack struct {
  stack []*errItem
}

func (se *ErrStack) Add(e error, msg string) {
  se.stack = append([]*errItem{{e: e, msg: msg}}, se.stack...)
}

func (se *ErrStack) HasErrors() bool {
  return len(se.stack) > 0
}

func (se *ErrStack) Error() string {
  if len(se.stack) > 0 {
    res := strings.Builder{}

    for _, v := range se.stack {
      _, _ = res.WriteString(v.msg + "\n")
      _, _ = res.WriteString("\t" + v.e.Error() + "\n")
    }

    return res.String()
  }

  return ""
}

func NewErrStack() ErrStacker {
  en := new(ErrStack)
  en.stack = make([]*errItem, 0, 10)

  return en
}
