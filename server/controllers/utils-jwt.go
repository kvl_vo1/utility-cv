package controllers

import (
  "fmt"
  "time"

  "github.com/dgrijalva/jwt-go"
  uuid "github.com/satori/go.uuid"
)

type ErrWrongSigningMethod struct {
  alg string
}

func (s *ErrWrongSigningMethod) Error() string {
  return fmt.Sprintf("unexpected signing method: %s", s.alg)
}

func NewErrWrongSigningMethod(sm interface{}) error {
  return &ErrWrongSigningMethod{alg: sm.(string)}
}

type JwtCustomClaims struct {
  UserID  string `json:"UserId"` // encrypted user _id
  Login   string `json:"login"`
  Name    string `json:"name"`
  IsAdmin bool   `json:"isAdmin"`
  jwt.StandardClaims
}

type JWTAuth struct {
  secretKey string
}

func NewJWT(secretKey string) *JWTAuth {
  ju := new(JWTAuth)
  ju.secretKey = secretKey

  return ju
}

func (j *JWTAuth) GenToken(login, name, userId string, isAdmin bool) (string, *JwtCustomClaims, error) {
  now := time.Now()

  claims := &JwtCustomClaims{
    UserID:  userId,
    Login:   login,
    Name:    name,
    IsAdmin: isAdmin,
    StandardClaims: jwt.StandardClaims{
      Id:        uuid.NewV4().String(),
      ExpiresAt: now.Add(time.Hour * 24).Unix(),
      IssuedAt:  now.Unix(),
      Issuer:    "utility.kvl.ua",
      Subject:   "Authenticating token for utility service.",
    },
  }

  token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)

  t, err := token.SignedString([]byte(j.secretKey))
  if err != nil {
    return "", nil, err
  }

  return t, claims, nil
}

func (j *JWTAuth) IsValid(t string) (bool, *JwtCustomClaims, error) {
  keyFunc := func(token *jwt.Token) (interface{}, error) {
    if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
      return nil, NewErrWrongSigningMethod(token.Header["alg"])
    }

    return []byte(j.secretKey), nil
  }

  // Parsing
  tkn, err := jwt.ParseWithClaims(t, &JwtCustomClaims{}, keyFunc)
  if err != nil {
    return false, nil, err
  }

  // Verifying
  claims, ok := tkn.Claims.(*JwtCustomClaims)
  if !ok && !tkn.Valid {
    return false, nil, err
  }

  return true, claims, nil
}
