package controllers

import (
  "sync"
  "time"

  uuid "github.com/satori/go.uuid"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

type ResetUser struct {
  UserId      primitive.ObjectID
  UUID        string
  NewPassword string
  expired     int64
}

type ResetMgr struct {
  m    sync.Mutex
  list map[string]*ResetUser
}

func NewResetMan() *ResetMgr {
  rm := &ResetMgr{
    m:    sync.Mutex{},
    list: make(map[string]*ResetUser, 4),
  }

  go rm.cleaner()

  return rm
}

func (rm *ResetMgr) cleaner() {
  timer := time.NewTicker(time.Second * 5)

  for true {
    <-timer.C
    rm.Clear()
  }
}

func (rm *ResetMgr) Clear() {
  rm.m.Lock()
  defer rm.m.Unlock()

  if len(rm.list) > 0 {
    tn := time.Now().Unix()

    for k := range rm.list {
      if rm.list[k].expired < tn {
        delete(rm.list, k)
      }
    }
  }
}

func (rm *ResetMgr) Empty() {
  rm.m.Lock()
  defer rm.m.Unlock()

  rm.list = make(map[string]*ResetUser, 4)
}

func (rm *ResetMgr) Add(userId primitive.ObjectID, login string) *ResetUser {
  rm.m.Lock()
  defer rm.m.Unlock()

  l, ok := rm.list[login]
  if ok {
    l.UserId = userId
    l.UUID = uuid.NewV4().String()
    l.NewPassword = MakeRandomPassword(8)
    l.expired = time.Now().Unix()
  } else {
    rm.list[login] = &ResetUser{
      UserId:      userId,
      UUID:        uuid.NewV4().String(),
      NewPassword: MakeRandomPassword(8),
      expired:     time.Now().Add(time.Minute * 5).Unix(),
    }
  }

  return rm.list[login]
}

func (rm *ResetMgr) DelByUUID(uuid string) {
  rm.m.Lock()
  defer rm.m.Unlock()

  for k, v := range rm.list {
    if v.UUID == uuid {
      delete(rm.list, k)
    }
  }
}

func (rm *ResetMgr) GetByUUID(chkId string) *ResetUser {
  rm.m.Lock()
  defer rm.m.Unlock()

  for _, v := range rm.list {
    if v.UUID == chkId {
      return v
    }
  }

  return nil
}
