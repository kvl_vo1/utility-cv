package controllers

import (
  "context"
  "encoding/json"
  "errors"
  "strings"
  "time"

  "utility/entity"

  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
  "go.mongodb.org/mongo-driver/mongo/readpref"
)

var (
  ErrWrongLogin          = errors.New("login must contain at least 4 characters")
  ErrWrongPassword       = errors.New("password must contain between 4-16 characters")
  ErrWrongFullName       = errors.New("full name must contain between 3-64 characters")
  ErrUserExists          = errors.New("user is already registered")
  ErrUserIsNotRegistered = errors.New("user is not registered")
  ErrBadEmail            = errors.New("bad login (wrong e-mail format)")
  ErrBadPassword         = errors.New("bad password")

  project = bson.M{
    "$project": bson.M{
      "_id":         0,
      "currReceipt": "$$ROOT",
    },
  }

  lookup = bson.M{
    "$lookup": bson.M{
      "from": "receipts",
      "let": bson.M{
        "uid": "$currReceipt.userId",
        "ppi": "$currReceipt.prevPeriodId",
      },
      "pipeline": bson.A{
        bson.M{
          "$match": bson.M{
            "$expr": bson.M{
              "$and": bson.A{
                bson.M{
                  "$eq": bson.A{"$userId", "$$uid"},
                },
                bson.M{
                  "$eq": bson.A{"$periodId", "$$ppi"},
                },
              },
            },
          },
        },
      },
      "as": "prevReceipts",
    },
  }
)

type MongoDB struct {
  client  *mongo.Client
  timeout time.Duration
  crypt   *Crypter
  Db      *mongo.Database
  ctx     context.Context
}

func NewMongoDB(dbPath string, crypt *Crypter) (*MongoDB, error) {
  m := new(MongoDB)
  m.timeout = time.Second * 10

  m.ctx = context.Background()

  m.crypt = crypt

  // Common context
  ctx, _ := context.WithTimeout(m.ctx, m.timeout)

  // Connection
  client, err := mongo.Connect(ctx, options.Client().ApplyURI(dbPath))
  if err != nil {
    return nil, err
  }

  // Checking connection
  err = client.Ping(ctx, readpref.Primary())
  if err != nil {
    return nil, err
  }

  m.client = client
  m.Db = client.Database("utility")

  // collection initialization
  if err := m.initCollections(); err != nil {
    return nil, err
  }

  return m, err
}

func (m *MongoDB) GetSettings() (*entity.Options, error) {
  c := m.Db.Collection("settings")

  var o entity.Options

  err := c.FindOne(m.ctx, bson.M{}, options.FindOne()).Decode(&o)

  if err != nil {
    return nil, err
  }

  return &o, nil
}

func (m *MongoDB) GetUserByLogin(login string) (*entity.User, error) {
  return m.getUser(bson.M{"login": login})
}

func (m *MongoDB) GetUserById(id primitive.ObjectID) (*entity.User, error) {
  return m.getUser(bson.M{"_id": id})
}

func (m *MongoDB) AddUserData(user *entity.User) ([]byte, error) {
  user.Id = primitive.NewObjectID()
  user.Login = strings.TrimSpace(user.Login)
  user.Login = strings.ToLower(user.Login)
  user.Password = strings.TrimSpace(user.Password)
  user.FullName = strings.TrimSpace(user.FullName)
  user.Flat = strings.TrimSpace(user.Flat)
  user.MaxConn = 10 // connections count per login
  user.PeriodId = 0
  user.IsAdmin = user.Login == "admin"
  user.CreatedAt = time.Now().Unix()

  // check if login exists
  _, err := m.GetUserByLogin(user.Login)
  if err != mongo.ErrNoDocuments {
    return nil, ErrUserExists
  }

  if user.IsAdmin {
    user.MaxConn = 0
  } else {
    err := IsEmailCorrect(user.Login)
    if err != nil {
      return nil, ErrBadEmail
    }
  }

  if len(user.FullName) < 3 || len(user.FullName) > 64 {
    return nil, ErrWrongFullName
  }

  if len(user.Login) < 4 {
    return nil, ErrWrongLogin
  }

  if len(user.Password) < 4 || len(user.Password) > 16 {
    return nil, ErrWrongPassword
  }

  // users collection
  collection := m.Db.Collection("users")

  password, err := GetHash(user.Password)
  if err != nil {
    return nil, err
  }
  user.Password = password

  ctx, _ := context.WithTimeout(m.ctx, m.timeout)
  newUser, err := collection.InsertOne(ctx, user)
  if err != nil {
    return nil, err
  }

  data, _ := json.Marshal(&newUser)

  return data, nil
}

func (m *MongoDB) GetReceiptByUserAndPeriod(userId primitive.ObjectID, periodId int64) (*entity.ReceiptEx, error) {
  return m.getReceipt(bson.M{
    "$match": bson.M{
      "userId":   userId,
      "periodId": periodId,
    },
  })
}

func (m *MongoDB) GetReceiptById(id primitive.ObjectID) (*entity.ReceiptEx, error) {
  return m.getReceipt(bson.M{
    "$match": bson.M{
      "_id": id,
    },
  })
}

func (m *MongoDB) GetReceiptsByUser(userId primitive.ObjectID) ([]entity.ReceiptEx, error) {
  c := m.Db.Collection("receipts")

  b := new([]entity.ReceiptEx)

  cursor, err := c.Aggregate(m.ctx, bson.A{
    bson.M{
      "$match": bson.M{
        "userId": userId,
      },
    },
    bson.M{
      "$sort": bson.M{
        "periodId": -1,
      },
    },
    bson.M{
      "$limit": 12,
    },
    project,
    lookup,
  }, options.Aggregate().SetAllowDiskUse(true))

  if err != nil {
    return nil, err
  }

  if err = cursor.All(m.ctx, b); err != nil {
    return nil, err
  }

  return *b, nil
}

func (m *MongoDB) ChangeUserPassword(userId primitive.ObjectID, password string) error {
  collection := *m.Db.Collection("users")
  ctx, _ := context.WithTimeout(m.ctx, m.timeout)

  filter := bson.M{"_id": userId}
  update := bson.D{{"$set", bson.M{"password": password}}}
  opts := options.Update()
  _, err := collection.UpdateOne(ctx, filter, update, opts)
  if err != nil {
    return err
  }

  return nil
}

func (m *MongoDB) InTransaction(tf func(mdb *mongo.Database, sctx mongo.SessionContext, params ...interface{}) error, params ...interface{}) error {
  ctx, _ := context.WithTimeout(m.ctx, m.timeout)

  fn := func(sessionContext mongo.SessionContext) error {
    // start
    err := sessionContext.StartTransaction()
    if err != nil {
      return err
    }

    e := NewErrStack()

    err = tf(m.Db, sessionContext, params...)
    if err != nil {
      e.Add(err, ">")

      // rollback
      err = sessionContext.AbortTransaction(sessionContext)
      if err != nil {
        e.Add(err, ">")
        return e
      }
    }

    if e.HasErrors() {
      return e
    }

    // commit
    err = sessionContext.CommitTransaction(sessionContext)
    if err != nil {
      return err
    }

    return nil
  }

  err := m.Db.Client().UseSession(ctx, fn)
  if err != nil {
    return err
  }

  return nil
}

func (m *MongoDB) GetFullReceiptById(id primitive.ObjectID) (*entity.Receipt, error) {
  receiptEx, err := m.GetReceiptById(id)
  if err != nil {
    return nil, err
  }

  receipt, err := m.ReceiptExToReceipt(receiptEx)
  if err != nil {
    return nil, err
  }

  return receipt, nil
}

func (m *MongoDB) initCollections() error {
  type Index struct {
    Name     string
    Fields   []string
    Desc     int
    IsUnique bool
  }

  cols := make(map[string][]Index, 10)
  cols["users"] = []Index{{
    Name:     "idx_users_password",
    Fields:   []string{"password"},
    Desc:     1,
    IsUnique: false,
  }, {
    Name:     "unq_idx_users_login",
    Fields:   []string{"login"},
    Desc:     1,
    IsUnique: true,
  }}

  cols["receipts"] = []Index{{
    Name:     "unq_idx_receipts_user_period_id",
    Fields:   []string{"userId", "periodId"},
    Desc:     -1,
    IsUnique: true,
  }, {
    Name:     "unq_idx_receipts_user_prev_period_id",
    Fields:   []string{"userId", "prevPeriodId"},
    Desc:     -1,
    IsUnique: true,
  }}

  //cols["files"] = []Index{{
  //  Name:     "idx_receiptId",
  //  Fields:   []string{"receiptId"},
  //  Desc:     1,
  //  IsUnique: false,
  //}}

  //cols["settings"] = []Index{{
  //  Name:     "idx_type",
  //  Fields:   []string{"type"},
  //  Desc:     -1,
  //  IsUnique: true,
  //}}

  existsCollections, err := m.Db.ListCollectionNames(m.ctx, bson.D{})
  if err != nil {
    return err
  }

  colExists := func(c string) bool {
    for _, v := range existsCollections {
      if v == c {
        return true
      }
    }

    return false
  }

  for name := range cols {
    if !colExists(name) && len(cols[name]) > 0 {
      collection := m.Db.Collection(name)

      il := cols[name]

      for i := range il {
        keys := make(bson.M, 10)

        // making index list
        for j := range il[i].Fields {
          keys[il[i].Fields[j]] = il[i].Desc
        }

        // unique
        opt := options.Index()
        if il[i].IsUnique {
          opt.SetUnique(true)
        }

        // index name
        if il[i].Name != "" {
          opt.SetName(il[i].Name)
        }

        var im mongo.IndexModel
        im.Keys = keys
        im.Options = opt

        ctx, _ := context.WithTimeout(m.ctx, m.timeout)
        _, err := collection.Indexes().CreateOne(ctx, im)
        if err != nil {
          return err
        }
      }
    }
  }

  // Settings
  if !colExists("settings") {
    password, _ := m.crypt.Encrypt("1Gk97yrzPffjLWFI")

    mnt := bson.D{
      {Key: "email", Value: bson.M{
        "recipient": "pokazvoda_lp@ukr.net",
        "server": bson.M{
          "name":     "Utility service",
          "host":     "smtp.ukr.net:465",
          "login":    "kvl_vo1@ukr.net",
          "password": password,
        },
      }},
    }

    ctx, _ := context.WithTimeout(m.ctx, m.timeout)
    _, err := m.Db.Collection("settings").InsertOne(ctx, mnt)
    if err != nil {
      return err
    }
  }

  return nil
}

func (m *MongoDB) getUser(filter interface{}) (*entity.User, error) {
  collection := *m.Db.Collection("users")
  ctx, _ := context.WithTimeout(m.ctx, m.timeout)

  var user entity.User
  res := collection.FindOne(ctx, filter)
  if res.Err() != nil {
    return nil, res.Err()
  }

  err := res.Decode(&user)
  if err != nil {
    return nil, err
  }

  // encrypting user _id
  ck, err := m.crypt.Encrypt(user.Id.Hex())
  if err != nil {
    return nil, err
  }

  // encrypted user _id
  user.UserId = ck

  return &user, nil
}

func (m *MongoDB) getReceipt(filter interface{}) (*entity.ReceiptEx, error) {
  c := m.Db.Collection("receipts")

  b := make([]entity.ReceiptEx, 0, 1)

  cursor, err := c.Aggregate(m.ctx, bson.A{
    filter,
    project,
    lookup,
  }, options.Aggregate().SetAllowDiskUse(true))

  if err != nil {
    return nil, err
  }

  if err = cursor.All(m.ctx, &b); err != nil {
    return nil, err
  }

  if len(b) == 0 {
    return nil, mongo.ErrNoDocuments
  }

  return &b[0], nil
}

func (m *MongoDB) ReceiptExToReceipt(be *entity.ReceiptEx) (*entity.Receipt, error) {
  res := entity.Receipt{}
  res = be.CurReceipt

  if res.IsInitial {
    return &res, nil
  }

  makeSlice := func(trg, src interface{}) []float64 {
    ar := make([]float64, 0, 2)
    ar = append(ar, trg.(float64))

    // попередній показник поточного періоду -
    // це початковий показник минулого періоду
    ar = append(ar, m.GetElem(src, 0))

    return ar
  }

  pb := be.Receipts[0]
  res.Electricity.Value = makeSlice(res.Electricity.Value, pb.Electricity.Value)
  res.Utility.ColdWater.Kitchen = makeSlice(res.Utility.ColdWater.Kitchen, pb.Utility.ColdWater.Kitchen)
  res.Utility.ColdWater.Bathroom = makeSlice(res.Utility.ColdWater.Bathroom, pb.Utility.ColdWater.Bathroom)
  res.Utility.HotWater.Kitchen = makeSlice(res.Utility.HotWater.Kitchen, pb.Utility.HotWater.Kitchen)
  res.Utility.HotWater.Bathroom = makeSlice(res.Utility.HotWater.Bathroom, pb.Utility.HotWater.Bathroom)

  return &res, nil
}

func (m *MongoDB) GetElem(src interface{}, index int) float64 {
  switch src.(type) {
  case float64:
    return src.(float64)
  case []interface{}:
    return src.([]interface{})[index].(float64)
  case primitive.A:
    return src.(primitive.A)[index].(float64)
  case []float64:
    return src.([]float64)[index]
  default:
    return 0
  }
}
