package controllers

import (
  "sync"
  "time"
)

type session struct {
  uuid    string
  expired int64
}

type sessions struct {
  maxCon int64
  items  []*session
}

type SessionMgr struct {
  m    sync.Mutex
  list map[string]*sessions
}

func NewSessionMan() *SessionMgr {
  sm := &SessionMgr{
    m:    sync.Mutex{},
    list: make(map[string]*sessions, 32),
  }

  go sm.cleaner()

  return sm
}

func (sm *SessionMgr) cleaner() {
  timer := time.NewTicker(time.Second * 5)

  for true {
    <-timer.C
    sm.Clear()
  }
}

func (sm *SessionMgr) Clear() {
  sm.m.Lock()
  defer sm.m.Unlock()

  tn := time.Now().Unix()

  for login, sess := range sm.list {
    for index := len(sess.items) - 1; index >= 0; index-- {
      if sess.items[index].expired < tn {
        sess.items = append(sess.items[:index], sess.items[index+1:]...)
      }
    }

    // delete all login sessions
    if len(sess.items) == 0 {
      delete(sm.list, login)
    }
  }
}

func (sm *SessionMgr) Empty() {
  sm.m.Lock()
  defer sm.m.Unlock()

  sm.list = make(map[string]*sessions, 32)
}

func (sm *SessionMgr) Add(login, uuid string, maxCon, exp int64) bool {
  sm.m.Lock()
  defer sm.m.Unlock()

  list, ok := sm.list[login]
  if ok {
    // login is PRESENT
    if (list.maxCon > 0) && int(list.maxCon) < (len(list.items)+1) {
      return false
    }

    list.items = append(list.items, &session{
      uuid:    uuid,
      expired: exp,
    })
  } else {
    // login is ABSENT
    sm.list[login] = &sessions{
      maxCon: maxCon,
      items: []*session{
        {
          uuid:    uuid,
          expired: exp,
        }},
    }
  }

  return true
}

func (sm *SessionMgr) Del(login, uuid string) {
  sm.m.Lock()
  defer sm.m.Unlock()

  list, ok := sm.list[login]
  if ok {
    for index := len(list.items) - 1; index >= 0; index-- {
      if list.items[index].uuid == uuid {
        list.items = append(list.items[:index], list.items[index+1:]...)
      }
    }

    // delete all login sessions
    if len(list.items) == 0 {
      delete(sm.list, login)
    }
  }
}
