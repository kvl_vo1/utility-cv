#! /bin/bash

mkdir "./bin"

rm -rf ./bin/client

cd ../client
npm run "clean \"build\""
npm run "prod"
cd ../server
cp -R ../client/build ./bin/client

go build -o ./bin/server
env GOOS=linux GOARCH=amd64 go build -o ./bin/server_linux
#cp ./server* ./bin

ln -s $(pwd)/bin/userdata ./bin/client/

rm -rf ./bin/client/assets/bootstrap
rm -rf ./bin/client/assets/materialize-css
rm -rf ./bin/client/assets/jquery
rm -rf ./bin/client/assets/owl

cd ./bin
zip -r -D ../bin.zip ./*
cd ../

mkdir -p "./bin/userdata"

#env UTILITY_HOST=192.168.0.110 ./server