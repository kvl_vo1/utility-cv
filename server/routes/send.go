package routes

import (
  "bytes"
  "fmt"
  "html/template"
  "net/http"
  "strconv"
  "strings"
  "time"

  "utility/controllers"
  "utility/entity"
  "utility/middlewares"

  "github.com/gin-gonic/gin"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "go.mongodb.org/mongo-driver/mongo"
)

const TABLE_TEMPLATE = `
<span style="display:block;background:#ffffff;font-family:Roboto, Arial, Geneva;">
  <div style="font-family:Roboto, Arial, Geneva;margin:0;padding:0;width:100%;">
    <table cellspacing="0" style="font-size: 12px; border-collapse: collapse;">
      <tbody style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline; background-color: transparent;">
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: center!important; border-width: 1px; font-weight: 700!important;"
            colspan="8">Показники по воді
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="2">П.І.П.
          </td>
          <td style="box-sizing: inherit; margin: 0px; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; background-color: #fffbeb; font-weight: 700 !important; text-align: right; vertical-align: middle;"
            colspan="6">{{ .Person.Name }}
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="2">№ квартири
          </td>
          <td style="box-sizing: inherit; margin: 0px; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font: inherit; text-align: inherit; background-color: #fffbeb; vertical-align: middle; font-weight: 700 !important; text-align: center;"
            colspan="3">{{ .Person.Flat }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: center!important; border-width: 1px;">
            Місяць
          </td>
          <td style="box-sizing: inherit; margin: 0px; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; background-color: #fffbeb; font-weight: 700 !important; text-align: center; vertical-align: middle;"
            colspan="2">{{ .Person.Month }}
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="5" rowspan="2">Споживання води за показниками лічильників
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: center!important; border-width: 1px;"
            colspan="3">Показники, куб. м
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            Поточні
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            Попередні
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px; font-weight: 700!important;">
            Спожито
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="2" rowspan="3">Холодна вода
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="3">1-й ліч(санвузол)
          </td>
          
          {{ $cb1 := index .ColdWater.Bathroom 0 }}
          {{ $cb2 := index .ColdWater.Bathroom 1 }}
          {{ $ck1 := index .ColdWater.Kitchen 0 }}
          {{ $ck2 := index .ColdWater.Kitchen 1 }}
      
          {{ $b := sub $cb1 $cb2 }}
          {{ $k := sub $ck1 $ck2 }}
      
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $cb1 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $cb2 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px; font-weight: 700!important;">
            {{ $b }}
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="3">2-й ліч(кухня)
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $ck1 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $ck2 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px; font-weight: 700!important;">
            {{ $k }}
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="3">Разом
          </td>
          <td style="box-sizing: inherit; margin: 0px; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; background-color: #fffbeb; font-weight: 700 !important; text-align: right; vertical-align: middle;"
            colspan="3">{{ sum $b $k }}
          </td>
        </tr>
      
        {{ $cb1 = index .HotWater.Bathroom 0 }}
        {{ $cb2 = index .HotWater.Bathroom 1 }}
        {{ $ck1 = index .HotWater.Kitchen 0 }}
        {{ $ck2 = index .HotWater.Kitchen 1 }}
      
        {{ $b = sub $cb1 $cb2 }}
        {{ $k = sub $ck1 $ck2 }}
      
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="2" rowspan="3">Гаряча вода
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="3">1-й ліч(санвузол)
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $cb1 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $cb2 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px; font-weight: 700!important;">
            {{ $b }}
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="3">2-й ліч(кухня)
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $ck1 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;">
            {{ $ck2 }}
          </td>
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px; font-weight: 700!important;">
            {{ $k }}
          </td>
        </tr>
        <tr style="box-sizing: inherit; margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;">
          <td style="box-sizing: inherit; margin: 0; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-size: 100%; font: inherit; vertical-align: middle!important; text-align: inherit; border-width: 1px;"
            colspan="3">Разом
          </td>
          <td style="box-sizing: inherit; margin: 0px; padding: 0.25em 0.5em; border: 1px solid #dbdbdb; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; background-color: #fffbeb; font-weight: 700 !important; text-align: right; vertical-align: middle;"
            colspan="3">{{ sum $b $k }}
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</span>
`

func Send(ctx *gin.Context) {
  var id = strings.TrimSpace(ctx.Param("id"))
  var periodId = int64(0)
  var err error

  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get("login")
  login := strings.Split(v.(string), " ")[0]

  v, _ = ctx.Get(middlewares.CtxCR)
  cr := v.(*controllers.Crypter)

  v, _ = ctx.Get(middlewares.CtxParams)
  params := v.(*entity.Params)

  pid, err := strconv.Atoi(id)
  if err != nil {
    ctx.Data(
      http.StatusBadRequest,
      gin.MIMEPlain,
      []byte(fmt.Sprintf("wrong param id \"%s\"", id)),
    )

    return
  }

  periodId = int64(pid)

  if !(controllers.IsPeriodRangeOk(periodId)) {
    ctx.Data(
      http.StatusBadRequest,
      gin.MIMEPlain,
      []byte(fmt.Sprintf("unsupported period \"%d\"", periodId)),
    )

    return
  }

  user, err := db.GetUserByLogin(login)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  // user has a current period
  receiptEx, err := db.GetReceiptByUserAndPeriod(user.Id, periodId)
  if err != nil {
    var s string

    if err == mongo.ErrNoDocuments {
      s = fmt.Sprintf("receipt for period #%d not found", user.PeriodId)
    } else {
      s = err.Error()
    }

    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(s))
    return
  }

  receipt, err := db.ReceiptExToReceipt(receiptEx)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  var table entity.TableData

  pd := controllers.ParsePeriodId(receipt.PeriodId)

  month := fmt.Sprintf("%s, %s", pd[0], pd[1])

  table.Person.Name = user.FullName
  table.Person.Flat = user.Flat
  table.Person.Month = month
  table.ColdWater = receipt.Utility.ColdWater
  table.HotWater = receipt.Utility.HotWater

  funcMap := template.FuncMap{
    "sum": func(a, b float64) float64 {
      return a + b
    },
    "sub": func(a, b float64) float64 {
      return a - b
    },
  }

  var buf bytes.Buffer

  t := template.Must(template.New("").Funcs(funcMap).Parse(TABLE_TEMPLATE))
  err = t.Execute(&buf, table)

  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  email := params.Options.EMail

  if email.Server.Password != "" {
    pwd, err := cr.Decrypt(email.Server.Password)

    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    email.Server.Password = pwd
  }

  sentAt := time.Now().Unix()

  err = controllers.SendEmail(
    email,
    fmt.Sprintf("Показники по воді %s. %s, кв: %s",
      month,
      user.FullName,
      user.Flat),
    buf.String(),
    []string{params.Options.EMail.Recipient})

  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  err = db.InTransaction(func(mdb *mongo.Database, sctx mongo.SessionContext, params ...interface{}) error {
    id := params[0].(primitive.ObjectID)
    sentAt := params[1].(int64)

    receipts := mdb.Collection("receipts")

    _, err := receipts.UpdateOne(
      sctx,
      bson.M{"_id": id},
      bson.M{"$set": bson.M{"utility.sentAt": sentAt}},
    )
    if err != nil {
      return err
    }

    return nil
  }, receipt.Id, sentAt)

  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  ctx.Data(http.StatusOK, gin.MIMEPlain, []byte(strconv.FormatInt(sentAt, 10)))
}
