package routes

import (
  "errors"
  "fmt"
  "log"
  "net/http"
  "os"
  "path"
  "path/filepath"
  "strconv"
  "strings"
  "time"

  "utility/controllers"
  "utility/entity"
  "utility/middlewares"

  "github.com/disintegration/imaging"
  "github.com/gin-gonic/gin"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
)

const (
  maskOriginalFile = "/%s/%s-original%s"
  maskThumbFile    = "/%s/%s-thumb%s"
)

func SendFiles(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get(middlewares.CtxParams)
  params := v.(*entity.Params)

  v, _ = ctx.Get("login")
  userId, _ := primitive.ObjectIDFromHex(strings.Split(v.(string), " ")[1])

  pid, err := strconv.Atoi(ctx.PostForm("periodId"))
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }
  periodId := int64(pid)

  subtype, ok := entity.ReceiptSubTypes[ctx.PostForm("type")]
  if !ok {
    ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(errors.New(fmt.Sprintf("unknown subtype: %s", ctx.PostForm("type"))).Error()))
    return
  }

  receiptEx, err := db.GetReceiptByUserAndPeriod(userId, periodId)
  if err != nil {
    var s string

    if err == mongo.ErrNoDocuments {
      s = fmt.Sprintf("receipt for period #%d not found", periodId)
    } else {
      s = err.Error()
    }

    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(s))
    return
  }

  form, err := ctx.MultipartForm()
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  udf := make(entity.UserFiles, 0, 25)

  files := form.File["files"]

  f := strings.Split(params.UserDataDir, "/")
  userdata := f[len(f)-1]

  for _, file := range files {
    id := primitive.NewObjectID()
    originalName := iifStr(len(file.Header["Content-Disposition"]) > 0, findValue("filename", file.Header["Content-Disposition"][0]), "")
    contentType := iifStr(len(file.Header["Content-Type"]) > 0, file.Header["Content-Type"][0], "")
    contentType = strings.ToLower(contentType)
    ext := path.Ext(originalName)
    prefix := id.Hex()[16:]

    var uf = entity.UserFile{
      Id:           id,
      ReceiptType:  subtype,
      OriginalName: originalName,
      OriginalPath: fmt.Sprintf(maskOriginalFile, userdata, prefix, ext),
      ThumbPath:    fmt.Sprintf(maskThumbFile, userdata, prefix, ext),
      ContentType:  contentType,
      Size:         file.Size,
      UploadDate:   time.Now().Unix(),
    }

    err = ctx.SaveUploadedFile(file, filepath.Join(params.RootDir, uf.OriginalPath))
    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    switch contentType {
    case "image/jpeg", "image/jpg", "image/png":
      f, _ := file.Open()
      srcImage, err := imaging.Decode(f)
      if err != nil {
        ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
        return
      }

      dstImageFit := imaging.Fit(srcImage, 64, 64, imaging.Lanczos)

      err = imaging.Save(dstImageFit, filepath.Join(params.RootDir, uf.ThumbPath), imaging.JPEGQuality(75))
      if err != nil {
        ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
        return
      }

    case "application/pdf":
      uf.ThumbPath = ""

    default:
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(fmt.Sprintf("unsupported content type: %s", contentType)))
      return
    }

    udf = append(udf, &uf)
  }

  // insert images metadata
  err = db.InTransaction(func(mdb *mongo.Database, sctx mongo.SessionContext, params ...interface{}) error {
    id := params[0].(primitive.ObjectID)
    subtype := params[1].(string)
    udf := params[2].(entity.UserFiles)

    m := make([]interface{}, len(udf))
    for i, v := range udf {
      m[i] = v
    }

    cReceipts := mdb.Collection("receipts")
    _, err = cReceipts.UpdateOne(
      sctx,
      bson.M{"_id": id},
      bson.M{
        "$addToSet": bson.M{
          fmt.Sprintf("%s.files", subtype): bson.M{"$each": m},
        },
      },
      options.Update().SetUpsert(true),
    )
    if err != nil {
      return err
    }

    return nil
  }, receiptEx.CurReceipt.Id, subtype, udf)

  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  rcp, err := db.GetFullReceiptById(receiptEx.CurReceipt.Id)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  ctx.JSON(http.StatusOK, rcp)
}

func RemoveFile(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get(middlewares.CtxParams)
  params := v.(*entity.Params)

  v, _ = ctx.Get("login")
  uid := strings.Split(v.(string), " ")[1]

  userId, _ := primitive.ObjectIDFromHex(uid)

  var rfi entity.RemoveFileItem
  if err := ctx.ShouldBindJSON(&rfi); err != nil {
    ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(fmt.Sprintf("wrong data for removing file: %s", err.Error())))
    return
  }

  err := db.InTransaction(func(mdb *mongo.Database, sctx mongo.SessionContext, params ...interface{}) error {
    file := params[0].(entity.RemoveFileItem)
    p := params[1].(*entity.Params)
    userId := params[2].(primitive.ObjectID)

    subtype, ok := entity.ReceiptSubTypes[file.ReceiptSubType]
    if !ok {
      return errors.New(fmt.Sprintf("unknown subtype: %s", file.ReceiptSubType))
    }

    cReceipts := mdb.Collection("receipts")

    res := cReceipts.FindOne(sctx, bson.M{"_id": file.ReceiptId, "userId": userId})
    if res.Err() != nil {
      return res.Err()
    }

    var receipt entity.Receipt
    err := res.Decode(&receipt)
    if err != nil {
      return err
    }

    _, err = cReceipts.UpdateOne(
      sctx,
      bson.M{"_id": file.ReceiptId},
      bson.M{
        "$pull": bson.M{
          fmt.Sprintf("%s.files", subtype): bson.M{
            "_id": file.FileId,
          },
        },
      },
      options.Update(),
    )
    if err != nil {
      return err
    }

    files, err := getFiles(rfi.ReceiptSubType, receipt)
    if err != nil {
      return err
    }

    // removing files
    for index := range files {
      if files[index].Id == file.FileId {
        if strings.TrimSpace(files[index].OriginalPath) != "" {
          file := path.Join(p.RootDir, files[index].OriginalPath)
          err := os.Remove(file)
          if err != nil {
            log.Println(err.Error())
          }
        }

        // для pdf - ThumbPath пустий
        if strings.TrimSpace(files[index].ThumbPath) != "" {
          file := path.Join(p.RootDir, files[index].ThumbPath)
          err = os.Remove(file)
          if err != nil {
            log.Println(err.Error())
          }
        }
      }
    }

    return nil
  }, rfi, params, userId)

  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  receipt, err := db.GetFullReceiptById(rfi.ReceiptId)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  ctx.JSON(http.StatusOK, receipt)
}

func getFiles(receiptSubType string, receipt entity.Receipt) (entity.UserFiles, error) {
  subtype := entity.ReceiptSubTypes[receiptSubType]
  files := make(entity.UserFiles, 0, 25)

  switch subtype {
  case entity.StElectricity:
    files = append(files, receipt.Electricity.Files...)

  case entity.StIntercom:
    files = append(files, receipt.Intercom.Files...)

  case entity.StInternet:
    files = append(files, receipt.Internet.Files...)

  case entity.StPhone:
    files = append(files, receipt.Phone.Files...)

  case entity.StUtility:
    files = append(files, receipt.Utility.Files...)

  default:
    return nil, errors.New(fmt.Sprintf("unknown subtype: %s", subtype))
  }

  return files, nil
}

func findValue(key string, str string) string {
  list := strings.Split(str, ";")

  for k := range list {
    list[k] = strings.TrimSpace(list[k])
  }

  kv := make(map[string]string)

  for i := range list {
    nd := strings.Split(list[i], "=")
    if len(nd) == 1 {
      kv[nd[0]] = ""
    } else if len(nd) == 2 {
      k := nd[0]
      v := strings.TrimSpace(nd[1])
      v = strings.TrimPrefix(v, "\"")
      v = strings.TrimSuffix(v, "\"")

      kv[k] = v
    }
  }

  res, ok := kv[key]
  if ok {
    return res
  }

  return ""
}

func iifStr(condition bool, isTrue, isFalse string) string {
  if condition {
    return isTrue
  }

  return isFalse
}
