package routes

import (
  "net/http"

  "utility/controllers"
  "utility/entity"
  "utility/middlewares"

  "github.com/gin-gonic/gin"
)

func SignUp(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  var nu entity.User
  if err := ctx.ShouldBindJSON(&nu); err != nil {
    ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte("Please input all fields"))
    return
  }

  _, err := db.AddUserData(&nu)
  if err != nil {
    switch err {
    case controllers.ErrWrongPassword, controllers.ErrWrongLogin, controllers.ErrUserExists, controllers.ErrBadEmail:
      ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte(err.Error()))
      return

    default:
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }
  }

  ctx.Status(http.StatusOK)
}
