package routes

import (
  "net/http"

  "utility/controllers"
  "utility/entity"
  "utility/middlewares"

  "github.com/gin-gonic/gin"
  "go.mongodb.org/mongo-driver/mongo"
)

func SignIn(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get(middlewares.CtxJWT)
  jwt := v.(*controllers.JWTAuth)

  v, _ = ctx.Get(middlewares.CtxSM)
  sm := v.(*controllers.SessionMgr)

  var u entity.User
  if err := ctx.ShouldBindJSON(&u); err != nil {
    ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte("Please input all fields"))
    return
  }

  acc, err := db.GetUserByLogin(u.Login)
  if err != nil {
    if err == mongo.ErrNoDocuments {
      ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte(controllers.ErrUserIsNotRegistered.Error()))
    } else {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    }

    return
  }

  ok, err := controllers.ComparePasswordByHash(u.Password, acc.Password)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  if !ok {
    ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte(controllers.ErrBadPassword.Error()))
    return
  }

  claims := &controllers.JwtCustomClaims{}
  token, claims, err := jwt.GenToken(acc.Login, acc.FullName, acc.UserId, acc.IsAdmin)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  if !sm.Add(claims.Login, claims.Id, acc.MaxConn, claims.ExpiresAt) {
    ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte("too many connections per login"))
    return
  }

  ctx.Data(http.StatusOK, gin.MIMEPlain, []byte(token))
}
