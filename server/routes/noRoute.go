package routes

import (
  "net/http"

  "github.com/gin-gonic/gin"
)

func NoRoute(c *gin.Context) {
  c.Redirect(http.StatusFound, "/")
}
