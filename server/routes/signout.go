package routes

import (
  "strings"

  "utility/controllers"
  "utility/middlewares"

  "github.com/gin-gonic/gin"
)

func SignOut(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxJWT)
  jwt := v.(*controllers.JWTAuth)

  v, _ = ctx.Get(middlewares.CtxSM)
  sm := v.(*controllers.SessionMgr)

  auth := strings.Split(strings.TrimSpace(ctx.GetHeader("Authorization")), " ")
  if len(auth) >= 2 && auth[1] != "" {
    claims := &controllers.JwtCustomClaims{}

    isValid, claims, err := jwt.IsValid(auth[1])
    if err == nil && claims != nil && isValid {
      sm.Del(claims.Login, claims.Id)
    }
  }
}
