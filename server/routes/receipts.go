package routes

import (
  "encoding/json"
  "fmt"
  "net/http"
  "strconv"
  "strings"
  "time"

  "utility/controllers"
  "utility/entity"
  "utility/middlewares"

  "github.com/gin-gonic/gin"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
)

func ReceiptsGet(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get("login")
  uid := strings.Split(v.(string), " ")[1]

  userId, _ := primitive.ObjectIDFromHex(uid)

  // []ReceiptEx
  be, err := db.GetReceiptsByUser(userId)
  if err != nil {
    if err == mongo.ErrNoDocuments {
      be = make([]entity.ReceiptEx, 0)
    } else {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }
  }

  b := make([]entity.Receipt, 0, len(be))
  for _, v := range be {
    pb, err := db.ReceiptExToReceipt(&v)
    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    b = append(b, *pb)
  }

  data, _ := json.MarshalIndent(&b, "", "  ")

  ctx.Data(http.StatusOK, gin.MIMEJSON, data)
}

func ReceiptAdd(ctx *gin.Context) {
  var id = strings.TrimSpace(ctx.Param("id"))
  var periodId = int64(0)
  var err error

  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get("login")
  login := strings.Split(v.(string), " ")[0]

  // checking range
  if id != "new" {
    pid, err := strconv.Atoi(id)
    if err != nil {
      ctx.Data(
        http.StatusBadRequest,
        gin.MIMEPlain,
        []byte(fmt.Sprintf("wrong param id \"%s\"", id)),
      )

      return
    }

    periodId = int64(pid)

    if !(controllers.IsPeriodRangeOk(periodId)) {
      ctx.Data(
        http.StatusBadRequest,
        gin.MIMEPlain,
        []byte(fmt.Sprintf("unsupported period \"%d\"", periodId)),
      )

      return
    }
  }

  // user info
  user, err := db.GetUserByLogin(login)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  if id == "new" {
    // user has no period
    if user.PeriodId <= 0 {
      periodId = controllers.MinPeriodId

      b := entity.Receipt{
        Id:           primitive.NewObjectID(),
        UserId:       user.Id,
        PeriodId:     periodId,
        PrevPeriodId: 0,
        Phone: entity.Phone{
          Total: 0,
          Files: entity.UserFiles{},
        },
        Internet: entity.Internet{
          Total: 0,
          Files: entity.UserFiles{},
        },
        Intercom: entity.Intercom{
          Total: 0,
          Files: entity.UserFiles{},
        },
        Electricity: entity.Electricity{
          Total: 0,
          Value: []float64{0, 0},
          Files: entity.UserFiles{},
        },
        Utility: entity.Utility{
          ColdWater: struct {
            Bathroom interface{} `json:"bathroom" bson:"bathroom"`
            Kitchen  interface{} `json:"kitchen" bson:"kitchen"`
          }{
            Bathroom: []float64{0, 0},
            Kitchen:  []float64{0, 0},
          },
          HotWater: struct {
            Bathroom interface{} `json:"bathroom" bson:"bathroom"`
            Kitchen  interface{} `json:"kitchen" bson:"kitchen"`
          }{
            Bathroom: []float64{0, 0},
            Kitchen:  []float64{0, 0},
          },
          Heating: struct {
            Total float64 `json:"total" bson:"total"`
            Price float64 `json:"price" bson:"price"`
            Value float64 `json:"value" bson:"value"`
          }{
            Total: 0,
            Price: 0,
            Value: 0,
          },
          Files: entity.UserFiles{},
        },
        IsInitial: true,
        IsSent:    false,
        IsLocked:  false,
        CreatedAt: time.Now().Unix(),
        UpdatedAt: 0,
      }

      err = db.InTransaction(func(mdb *mongo.Database, sctx mongo.SessionContext, params ...interface{}) error {
        receipt := params[0].(entity.Receipt)
        userId := receipt.UserId
        periodId := receipt.PeriodId

        users := mdb.Collection("users")
        receipts := mdb.Collection("receipts")

        _, err = receipts.DeleteOne(sctx, bson.D{
          bson.E{
            Key:   "userId",
            Value: userId,
          },
          bson.E{
            Key:   "periodId",
            Value: periodId,
          },
        })
        if err != nil {
          return err
        }

        _, err := users.UpdateOne(
          sctx,
          bson.M{"_id": userId},
          bson.M{"$set": bson.M{"periodId": periodId}},
        )
        if err != nil {
          return err
        }

        _, err = receipts.InsertOne(sctx, receipt)
        if err != nil {
          return err
        }

        return nil
      }, b)
    } else {
      // user has a current period
      cb, err := db.GetReceiptByUserAndPeriod(user.Id, user.PeriodId)
      if err != nil {
        var s string

        if err == mongo.ErrNoDocuments {
          s = fmt.Sprintf("receipt for period #%d not found", user.PeriodId)
        } else {
          s = err.Error()
        }

        ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(s))
        return
      }

      periodId, err = controllers.NextPeriodId(user.PeriodId)
      if err != nil {
        ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(err.Error()))
        return
      }

      // insert new receipt
      receipt := &entity.Receipt{
        Id:           primitive.NewObjectID(),
        UserId:       user.Id,
        PeriodId:     periodId,
        PrevPeriodId: user.PeriodId,
        Phone: entity.Phone{
          Total: 0,
          Files: entity.UserFiles{},
        },
        Internet: entity.Internet{
          Total: 0,
          Files: entity.UserFiles{},
        },
        Intercom: entity.Intercom{
          Total: 0,
          Files: entity.UserFiles{},
        },
        Electricity: entity.Electricity{
          Total: 0,
          Value: db.GetElem(cb.CurReceipt.Electricity.Value, 0),
          Files: entity.UserFiles{},
        },
        Utility: entity.Utility{
          ColdWater: struct {
            Bathroom interface{} `json:"bathroom" bson:"bathroom"`
            Kitchen  interface{} `json:"kitchen" bson:"kitchen"`
          }{
            Bathroom: db.GetElem(cb.CurReceipt.Utility.ColdWater.Bathroom, 0),
            Kitchen:  db.GetElem(cb.CurReceipt.Utility.ColdWater.Kitchen, 0),
          },
          HotWater: struct {
            Bathroom interface{} `json:"bathroom" bson:"bathroom"`
            Kitchen  interface{} `json:"kitchen" bson:"kitchen"`
          }{
            Bathroom: db.GetElem(cb.CurReceipt.Utility.HotWater.Bathroom, 0),
            Kitchen:  db.GetElem(cb.CurReceipt.Utility.HotWater.Kitchen, 0),
          },
          Heating: struct {
            Total float64 `json:"total" bson:"total"`
            Price float64 `json:"price" bson:"price"`
            Value float64 `json:"value" bson:"value"`
          }{
            Total: 0,
            Price: 0,
            Value: 0,
          },
          Files: entity.UserFiles{},
        },
        IsInitial: false,
        IsSent:    false,
        IsLocked:  false,
        CreatedAt: time.Now().Unix(),
        UpdatedAt: 0,
      }

      err = db.InTransaction(func(mdb *mongo.Database, sctx mongo.SessionContext, params ...interface{}) error {
        receipt := params[0].(*entity.Receipt)
        users := mdb.Collection("users")
        receipts := mdb.Collection("receipts")

        _, err := users.UpdateOne(
          sctx,
          bson.M{"_id": receipt.UserId},
          bson.M{"$set": bson.M{"periodId": receipt.PeriodId}},
        )
        if err != nil {
          return err
        }

        _, err = receipts.UpdateOne(
          sctx,
          bson.M{"userId": receipt.UserId, "periodId": receipt.PrevPeriodId},
          bson.M{"$set": bson.M{"isLocked": true}},
        )
        if err != nil {
          return err
        }

        _, err = receipts.InsertOne(sctx, receipt)
        if err != nil {
          return err
        }

        return nil
      }, receipt)
    }
  } else {
    // 20YYMM
    var oldReceiptEx *entity.ReceiptEx
    var newReceipt entity.Receipt
    if err := ctx.ShouldBindJSON(&newReceipt); err != nil {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    if periodId != newReceipt.PeriodId {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte("url period and data period are different."))
      return
    }

    oldReceiptEx, err := db.GetReceiptById(newReceipt.Id)
    if err != nil {
      var s string
      if err == mongo.ErrNoDocuments {
        s = fmt.Sprintf("receipt for period #%d not found", periodId)
      } else {
        s = err.Error()
      }

      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(s))
      return
    }

    oldReceipt, err := db.ReceiptExToReceipt(oldReceiptEx)
    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    if oldReceipt.Id != newReceipt.Id {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte("oldId != newId"))
      return
    }

    // заблоковані рахунки забороненмо міняти
    if oldReceipt.IsLocked {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte("locked receipt is unable to change."))
      return
    }

    // ці поля на клієнті можуть поміняти,
    // тому їх краще скопіювати з попереднього рахунку
    newReceipt.UserId = oldReceipt.UserId
    newReceipt.PeriodId = oldReceipt.PeriodId
    newReceipt.PrevPeriodId = oldReceipt.PrevPeriodId
    newReceipt.IsInitial = oldReceipt.IsInitial
    newReceipt.IsSent = oldReceipt.IsSent
    newReceipt.IsLocked = oldReceipt.IsLocked
    newReceipt.CreatedAt = oldReceipt.CreatedAt
    newReceipt.UpdatedAt = time.Now().Unix()

    err = checkReceipt(&newReceipt)
    if err != nil {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    err = checkNewAndPrev(&newReceipt, oldReceipt)
    if err != nil {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    c := db.Db.Collection("receipts")
    filter := bson.M{"_id": newReceipt.Id}
    update := bson.D{{"$set", &newReceipt}}
    opts := options.Update() //.SetUpsert(true)
    _, err = c.UpdateOne(ctx, filter, update, opts)
    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    ctx.Status(http.StatusOK)
    return
  }

  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  ctx.Data(http.StatusOK, gin.MIMEPlain, []byte(strconv.FormatInt(periodId, 10)))
}

func ReceiptGet(ctx *gin.Context) {
  var id = strings.TrimSpace(ctx.Param("id"))

  pid, err := strconv.Atoi(id)
  if err != nil {
    ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(fmt.Sprintf("wrong param id \"%s\"", id)))
    return
  }

  periodId := int64(pid)
  if ok := controllers.IsPeriodRangeOk(periodId); !ok {
    ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(fmt.Sprintf("unsupported period \"%d\"", periodId)))
    return
  }

  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get("login")
  uid := strings.Split(v.(string), " ")[1]

  userId, _ := primitive.ObjectIDFromHex(uid)

  receiptEx, err := db.GetReceiptByUserAndPeriod(userId, periodId)
  if err != nil {
    var s string

    if err == mongo.ErrNoDocuments {
      s = fmt.Sprintf("receipt for period #%d not found", periodId)
    } else {
      s = err.Error()
    }

    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(s))
    return
  }

  receipt, err := db.ReceiptExToReceipt(receiptEx)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  data, _ := json.MarshalIndent(&receipt, "", "  ")

  ctx.Data(http.StatusOK, gin.MIMEJSON, data)
}

func checkNewAndPrev(new, old *entity.Receipt) error {
  if new.IsInitial {
    return nil
  }

  const mask = "%s: new value(%f) is less than previous one(%f)"
  var vNew, vOld float64
  var err error

  getVal := func(i interface{}, index int) (float64, error) {
    switch v := i.(type) {
    case []interface{}:
      return v[index].(float64), nil

    case []float64:
      return v[index], nil

    case []int64:
      return float64(v[index]), nil

    case []int32:
      return float64(v[index]), nil

    case float64:
      return v, nil
    }

    return 0.0, fmt.Errorf("value is not '[]float64' or 'float64'")
  }

  // Electricity
  vNew, err = getVal(new.Electricity.Value, 0)
  if err != nil {
    return err
  }
  vOld, err = getVal(old.Electricity.Value, 1)
  if err != nil {
    return err
  }
  if vNew < vOld {
    return fmt.Errorf(mask, "Electricity", vNew, vOld)
  }

  // ColdWater.Bathroom
  vNew, err = getVal(new.Utility.ColdWater.Bathroom, 0)
  if err != nil {
    return err
  }
  vOld, err = getVal(old.Utility.ColdWater.Bathroom, 1)
  if err != nil {
    return err
  }
  if vNew < vOld {
    return fmt.Errorf(mask, "ColdWater.Bathroom", vNew, vOld)
  }

  // ColdWater.Kitchen
  vNew, err = getVal(new.Utility.ColdWater.Kitchen, 0)
  if err != nil {
    return err
  }
  vOld, err = getVal(old.Utility.ColdWater.Kitchen, 1)
  if err != nil {
    return err
  }
  if vNew < vOld {
    return fmt.Errorf(mask, "ColdWater.Kitchen", vNew, vOld)
  }

  // HotWater.Bathroom
  vNew, err = getVal(new.Utility.HotWater.Bathroom, 0)
  if err != nil {
    return err
  }
  vOld, err = getVal(old.Utility.HotWater.Bathroom, 1)
  if err != nil {
    return err
  }
  if vNew < vOld {
    return fmt.Errorf(mask, "HotWater.Bathroom", vNew, vOld)
  }

  // HotWater.Kitchen
  vNew, err = getVal(new.Utility.HotWater.Kitchen, 0)
  if err != nil {
    return err
  }
  vOld, err = getVal(old.Utility.HotWater.Kitchen, 1)
  if err != nil {
    return err
  }
  if vNew < vOld {
    return fmt.Errorf(mask, "HotWater.Kitchen", vNew, vOld)
  }

  // конвертація масиву флоатів в один флоат
  if !new.IsInitial {
    new.Electricity.Value = new.Electricity.Value.([]interface{})[0].(float64)
    new.Utility.ColdWater.Bathroom = new.Utility.ColdWater.Bathroom.([]interface{})[0].(float64)
    new.Utility.ColdWater.Kitchen = new.Utility.ColdWater.Kitchen.([]interface{})[0].(float64)
    new.Utility.HotWater.Bathroom = new.Utility.HotWater.Bathroom.([]interface{})[0].(float64)
    new.Utility.HotWater.Kitchen = new.Utility.HotWater.Kitchen.([]interface{})[0].(float64)
  }

  return nil
}

func checkReceipt(b *entity.Receipt) error {
  const errTmp = `"%s" is wrong`

  convToSlice := func(in interface{}) []interface{} {
    a := make([]interface{}, 0, 2)

    switch in.(type) {
    case primitive.A:
      b := in.(primitive.A)
      for _, v := range b {
        a = append(a, v)
      }
      return a

    case float64:
      b := in.(float64)
      a = append(a, b, float64(0))
      return a

    default:
      return in.([]interface{})
    }
  }

  b.Electricity.Value = convToSlice(b.Electricity.Value)
  if v, ok := b.Electricity.Value.([]interface{}); !ok || len(v) != 2 || (v[0].(float64) < v[1].(float64)) {
    return fmt.Errorf(errTmp, "Electricity.Value")
  }

  b.Utility.ColdWater.Bathroom = convToSlice(b.Utility.ColdWater.Bathroom)
  if v, ok := b.Utility.ColdWater.Bathroom.([]interface{}); !ok || len(v) != 2 || (v[0].(float64) < v[1].(float64)) {
    return fmt.Errorf(errTmp, "Utility.ColdWater.Bathroom")
  }

  b.Utility.ColdWater.Kitchen = convToSlice(b.Utility.ColdWater.Kitchen)
  if v, ok := b.Utility.ColdWater.Kitchen.([]interface{}); !ok || len(v) != 2 || (v[0].(float64) < v[1].(float64)) {
    return fmt.Errorf(errTmp, "Utility.ColdWater.Kitchen")
  }

  b.Utility.HotWater.Bathroom = convToSlice(b.Utility.HotWater.Bathroom)
  if v, ok := b.Utility.HotWater.Bathroom.([]interface{}); !ok || len(v) != 2 || (v[0].(float64) < v[1].(float64)) {
    return fmt.Errorf(errTmp, "Utility.HotWater.Bathroom")
  }

  b.Utility.HotWater.Kitchen = convToSlice(b.Utility.HotWater.Kitchen)
  if v, ok := b.Utility.HotWater.Kitchen.([]interface{}); !ok || len(v) != 2 || (v[0].(float64) < v[1].(float64)) {
    return fmt.Errorf(errTmp, "Utility.HotWater.Kitchen")
  }

  return nil
}
