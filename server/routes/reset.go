package routes

import (
  "bytes"
  "fmt"
  "html/template"
  "net/http"
  "net/url"

  "utility/controllers"
  "utility/entity"
  "utility/middlewares"

  "github.com/gin-gonic/gin"
)

const (
  PASSWORD = `
    <p>
      You are about to reset the password for login <span style="color: red;">{{ .Login }}</span>.
      <br>
      <a href="{{ .Url}}">Click here to continue</a>
    </p>
  `

  NEW = `
    <p>
      Your new credentials:<br>
      Login: {{ .Login }}<br>
      Password: <span style="color: red">{{ .Password }}</span><br>
    </p>
  `
)

func Reset(ctx *gin.Context) {
  login := ctx.Query("login")
  verify := ctx.Query("verify")

  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get(middlewares.CtxRM)
  rm := v.(*controllers.ResetMgr)

  v, _ = ctx.Get(middlewares.CtxCR)
  cr := v.(*controllers.Crypter)

  v, _ = ctx.Get(middlewares.CtxParams)
  params := v.(*entity.Params)

  email := params.Options.EMail

  if email.Server.Password != "" {
    pwd, err := cr.Decrypt(email.Server.Password)

    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    email.Server.Password = pwd
  }

  type Reset struct {
    Login    string
    Password string
    Url      string
  }

  tmp := template.New("")

  if login != "" {
    err := controllers.IsEmailCorrect(login)
    if err != nil {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    u, err := db.GetUserByLogin(login)
    if err != nil {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(fmt.Sprintf("login \"%s\" not found", login)))
      return
    }

    r := rm.Add(u.Id, u.Login)

    resetUrl := fmt.Sprintf("%s/api/reset?verify=%s", params.GetAddr("http://"), url.QueryEscape(r.UUID))

    var tpl bytes.Buffer
    t := template.Must(tmp.Parse(PASSWORD))
    _ = t.Execute(&tpl, Reset{
      Login:    u.Login,
      Password: "",
      Url:      resetUrl,
    })
    body := tpl.String()

    err = controllers.SendEmail(email, "Resetting password", body, []string{u.Login})
    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    ctx.Status(http.StatusOK)
    return
  }

  if verify != "" {
    r := rm.GetByUUID(verify)
    if r != nil {
      hp, _ := controllers.GetHash(r.NewPassword)
      err := db.ChangeUserPassword(r.UserId, hp)
      if err != nil {
        ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
        return
      }

      u, err := db.GetUserById(r.UserId)
      if err != nil {
        ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(fmt.Sprintf("login linked with UUID \"%s\" not found", r.UUID)))
        return
      }

      var tpl bytes.Buffer
      t := template.Must(tmp.Parse(NEW))
      _ = t.Execute(&tpl, Reset{
        Login:    u.Login,
        Password: r.NewPassword,
        Url:      "",
      })
      body := tpl.String()

      err = controllers.SendEmail(email, "New credentials", body, []string{u.Login})
      if err != nil {
        ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
        return
      }

      rm.DelByUUID(verify)

      ctx.Status(http.StatusOK)
      return
    } else {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte(fmt.Sprintf("unknown reset key: %s", verify)))
      return
    }
  }

  ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte("query param login/verify is empty or not found"))
}
