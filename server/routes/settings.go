package routes

import (
  "encoding/json"
  "net/http"
  "strings"

  "utility/controllers"
  "utility/entity"
  "utility/middlewares"

  "github.com/gin-gonic/gin"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "go.mongodb.org/mongo-driver/mongo"
)

func SettingsLoad(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get("login")
  login := strings.Split(v.(string), " ")[0]

  user, err := db.GetUserByLogin(login)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  var settings entity.Settings

  settings.User = *user

  if user.IsAdmin {
    o, err := db.GetSettings()
    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      return
    }

    settings.Email = &o.EMail
    settings.Email.Server.Password = ""
  }

  settings.User.Id = primitive.NilObjectID
  settings.User.Password = ""
  settings.User.UserId = ""

  b, _ := json.Marshal(&settings)

  ctx.Data(http.StatusOK, gin.MIMEPlain, b)
}

func SettingsSave(ctx *gin.Context) {
  v, _ := ctx.Get(middlewares.CtxNameDB)
  db := v.(*controllers.MongoDB)

  v, _ = ctx.Get("login")
  login := strings.Split(v.(string), " ")[0]

  v, _ = ctx.Get(middlewares.CtxCR)
  cr := v.(*controllers.Crypter)

  user, err := db.GetUserByLogin(login)
  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  var settings entity.Settings
  if err := ctx.ShouldBindJSON(&settings); err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  settings.User.Id = user.Id

  err = db.InTransaction(func(mdb *mongo.Database, sctx mongo.SessionContext, params ...interface{}) error {
    settings := params[0].(entity.Settings)
    cUsers := mdb.Collection("users")
    cSettings := mdb.Collection("settings")

    _, err := cUsers.UpdateOne(
      sctx,
      bson.M{"_id": settings.User.Id},
      bson.M{"$set": bson.M{
        "fullName": settings.User.FullName,
        "flat":     settings.User.Flat,
      }},
    )
    if err != nil {
      return err
    }

    if settings.User.IsAdmin {
      q := bson.M{
        "email.recipient":    settings.Email.Recipient,
        "email.server.host":  settings.Email.Server.Host,
        "email.server.login": settings.Email.Server.Login,
      }

      pwd := strings.TrimSpace(settings.Email.Server.Password)

      if pwd != "" {
        pwd, _ := cr.Encrypt(pwd)

        q["email.server.password"] = pwd
      }

      _, err = cSettings.UpdateOne(sctx, bson.D{}, bson.M{"$set": q})
      if err != nil {
        return err
      }
    }

    return nil
  }, settings)

  if err != nil {
    ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
    return
  }

  ctx.Status(http.StatusOK)
}
