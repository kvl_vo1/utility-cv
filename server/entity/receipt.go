package entity

import (
  "go.mongodb.org/mongo-driver/bson/primitive"
)

const (
  StElectricity = "electricity"
  StIntercom    = "intercom"
  StInternet    = "internet"
  StPhone       = "phone"
  StUtility     = "utility"
)

var ReceiptSubTypes = map[string]string{
  "ELECTRICITY": StElectricity,
  "INTERCOM":    StIntercom,
  "INTERNET":    StInternet,
  "PHONE":       StPhone,
  "WATER":       StUtility,
}

type Phone struct {
  Total float64   `json:"total" bson:"total"`
  Files UserFiles `json:"files" bson:"files"`
}

type Internet struct {
  Total float64   `json:"total" bson:"total"`
  Files UserFiles `json:"files" bson:"files"`
}

type Intercom struct {
  Total float64   `json:"total" bson:"total"`
  Files UserFiles `json:"files" bson:"files"`
}

type Electricity struct {
  Total float64     `json:"total" bson:"total"`
  Value interface{} `json:"value" bson:"value"`
  Files UserFiles   `json:"files" bson:"files"`
}

type ColdWater struct {
  Bathroom interface{} `json:"bathroom" bson:"bathroom"`
  Kitchen  interface{} `json:"kitchen" bson:"kitchen"`
}

type HotWater struct {
  Bathroom interface{} `json:"bathroom" bson:"bathroom"`
  Kitchen  interface{} `json:"kitchen" bson:"kitchen"`
}

type Heating struct {
  Total float64 `json:"total" bson:"total"`
  Price float64 `json:"price" bson:"price"`
  Value float64 `json:"value" bson:"value"`
}

type Utility struct {
  ColdWater ColdWater `json:"coldWater" bson:"coldWater"`
  HotWater  HotWater  `json:"hotWater" bson:"hotWater"`
  Heating   struct {
    Total float64 `json:"total" bson:"total"`
    Price float64 `json:"price" bson:"price"`
    Value float64 `json:"value" bson:"value"`
  } `json:"heating" bson:"heating"`
  SentAt int64     `json:"sentAt" bson:"sentAt"`
  Files  UserFiles `json:"files" bson:"files"`
}

type Receipt struct {
  Id           primitive.ObjectID `json:"_id" bson:"_id"`
  UserId       primitive.ObjectID `json:"-" bson:"userId"`
  PeriodId     int64              `json:"periodId" bson:"periodId"`
  PrevPeriodId int64              `json:"prevPeriodId" bson:"prevPeriodId"`
  Phone        Phone              `json:"phone" bson:"phone"`
  Internet     Internet           `json:"internet" bson:"internet"`
  Intercom     Intercom           `json:"intercom" bson:"intercom"`
  Electricity  Electricity        `json:"electricity" bson:"electricity"`
  Utility      Utility            `json:"utility" bson:"utility"`
  IsInitial    bool               `json:"isInitial" bson:"isInitial"`
  IsSent       bool               `json:"isSent" bson:"isSent"`
  IsLocked     bool               `json:"isLocked" bson:"isLocked"`
  CreatedAt    int64              `json:"createdAt" bson:"createdAt"`
  UpdatedAt    int64              `json:"updatedAt" bson:"updatedAt"`
}

type ReceiptEx struct {
  CurReceipt Receipt   `json:"currReceipt" bson:"currReceipt"`
  Receipts   []Receipt `json:"prevReceipts" bson:"prevReceipts"`
}
