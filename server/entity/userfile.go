package entity

import "go.mongodb.org/mongo-driver/bson/primitive"

type UserFile struct {
  Id           primitive.ObjectID `json:"_id" bson:"_id"`
  ReceiptType  string             `json:"receiptType" bson:"receiptType"`
  OriginalName string             `json:"originalName" bson:"originalName"`
  OriginalPath string             `json:"originalPath" bson:"originalPath"`
  ThumbPath    string             `json:"thumbPath" bson:"thumbPath"`
  ContentType  string             `json:"contentType" bson:"contentType"`
  Size         int64              `json:"size" bson:"size"`
  UploadDate   int64              `json:"uploadDate" bson:"uploadDate"`
}

type UserFiles []*UserFile

type RemoveFileItem struct {
  ReceiptId      primitive.ObjectID `json:"receiptId"`
  ReceiptSubType string             `json:"receiptSubType"`
  FileId         primitive.ObjectID `json:"fileId"`
}
