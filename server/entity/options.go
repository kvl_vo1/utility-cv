package entity

type EMail struct {
  Recipient string `json:"recipient" bson:"recipient"`
  Server    struct {
    Name     string `json:"name" bson:"name"`
    Host     string `json:"host" bson:"host"`
    Login    string `json:"login" bson:"login"`
    Password string `json:"password,omitempty" bson:"password,omitempty"`
  } `json:"server" bson:"server"`
}

type Options struct {
  EMail EMail `json:"email" bson:"email"`
}
