package entity

type Month struct {
  Id   int64  `json:"_id" bson:"_id"`
  Name string `json:"name" bson:"name"`
}
