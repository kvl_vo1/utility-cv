package entity

import "fmt"

type Params struct {
  Host        string
  Port        int
  SecretKey   string
  DbPath      string
  RootDir     string
  UserDataDir string
  Options     Options
}

func (p *Params) GetAddr(proto string) string {
  return fmt.Sprintf("%s%s:%d", proto, p.Host, p.Port)
}
