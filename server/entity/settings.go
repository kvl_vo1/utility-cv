package entity

type Settings struct {
  User  User   `json:"user" bson:"user"`
  Email *EMail `json:"email,omitempty" bson:"email,omitempty"`
}
