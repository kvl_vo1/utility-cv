package entity

type TableData struct {
  Person struct {
    Name  string `json:"name" bson:"name"`
    Flat  string `json:"flat" bson:"flat"`
    Month string `json:"month" bson:"month"`
  } `json:"person" bson:"person"`
  ColdWater ColdWater `json:"coldWater" bson:"coldWater"`
  HotWater  HotWater  `json:"hotWater" bson:"hotWater"`
}
