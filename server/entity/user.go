package entity

import (
  "go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
  Id        primitive.ObjectID `json:"_id" bson:"_id"`
  UserId    string             `json:"userId,omitempty" bson:"-"`
  Login     string             `json:"login" bson:"login"`
  Password  string             `json:"password,omitempty" bson:"password,omitempty"`
  FullName  string             `json:"fullName" bson:"fullName"`
  Flat      string             `json:"flat" bson:"flat"`
  MaxConn   int64              `json:"maxConn" bson:"maxConn"`
  IsAdmin   bool               `json:"isAdmin" bson:"isAdmin"`
  PeriodId  int64              `json:"periodId" bson:"periodId"`
  CreatedAt int64              `json:"createdAt" bson:"createdAt"`
}
