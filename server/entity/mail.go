package entity

type Mail struct {
  Host     string `json:"host" bson:"host"`
  Login    string `json:"login" bson:"login"`
  Password string `json:"password" bson:"password"`
  Name     string `json:"name" bson:"name"`
}
