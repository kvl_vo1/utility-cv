package middlewares

import (
  "github.com/gin-contrib/static"
  "github.com/gin-gonic/gin"
)

func StaticFiles(rootDir string) gin.HandlerFunc {
  return static.Serve("/", static.LocalFile(rootDir, true))
}
