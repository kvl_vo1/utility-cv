package middlewares

import (
  "fmt"
  "net/http"

  "utility/controllers"
  "utility/entity"

  "github.com/gin-gonic/gin"
)

const (
  CtxParams = "pr"  // Params
  CtxNameDB = "db"  // MongoDB
  CtxJWT    = "jwt" // JWT
  CtxSM     = "sm"  // Session Manager
  CtxRM     = "rm"  // Reset Password Manager
  CtxCR     = "cr"  // Crypter
)

func AssignCtx(db *controllers.MongoDB, jwt *controllers.JWTAuth, sm *controllers.SessionMgr, cr *controllers.Crypter, rm *controllers.ResetMgr, pr *entity.Params) gin.HandlerFunc {
  return func(ctx *gin.Context) {
    ctx.Set(CtxParams, pr)
    ctx.Set(CtxNameDB, db)
    ctx.Set(CtxJWT, jwt)
    ctx.Set(CtxSM, sm)
    ctx.Set(CtxRM, rm)
    ctx.Set(CtxCR, cr)
  }
}

func CheckCtx() gin.HandlerFunc {
  return func(ctx *gin.Context) {
    v, ok := ctx.Get(CtxParams)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is empty", CtxParams)))
      ctx.Abort()
      return
    }

    _, ok = v.(*entity.Params)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is is not \"*entity.Params\"", CtxParams)))
      ctx.Abort()
      return
    }

    v, ok = ctx.Get(CtxNameDB)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is empty", CtxNameDB)))
      ctx.Abort()
      return
    }

    _, ok = v.(*controllers.MongoDB)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is is not \"*controllers.MongoDB\"", CtxNameDB)))
      ctx.Abort()
      return
    }

    v, ok = ctx.Get(CtxJWT)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is empty", CtxJWT)))
      ctx.Abort()
      return
    }

    _, ok = v.(*controllers.JWTAuth)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is is not \"*controllers.JWTAuth\"", CtxJWT)))
      ctx.Abort()
      return
    }

    v, ok = ctx.Get(CtxSM)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is empty", CtxSM)))
      ctx.Abort()
      return
    }

    _, ok = v.(*controllers.SessionMgr)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is is not \"*controllers.SessionMgr\"", CtxSM)))
      ctx.Abort()
      return
    }

    v, ok = ctx.Get(CtxRM)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is empty", CtxRM)))
      ctx.Abort()
      return
    }

    _, ok = v.(*controllers.ResetMgr)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is is not \"*controllers.ResetMgr\"", CtxRM)))
      ctx.Abort()
      return
    }

    v, ok = ctx.Get(CtxCR)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is empty", CtxCR)))
      ctx.Abort()
      return
    }

    _, ok = v.(*controllers.Crypter)
    if !ok {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(fmt.Sprintf("ctx[\"%s\"] is is not \"*controllers.Crypter\"", CtxCR)))
      ctx.Abort()
      return
    }
  }
}
