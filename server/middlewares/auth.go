package middlewares

import (
  "errors"
  "fmt"
  "net/http"
  "strings"

  "utility/controllers"

  "github.com/dgrijalva/jwt-go"
  "github.com/gin-gonic/gin"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

var errNeedToLogin = errors.New("need to login first")

func AuthJWT() gin.HandlerFunc {
  return func(ctx *gin.Context) {
    v, _ := ctx.Get(CtxJWT)
    ja := v.(*controllers.JWTAuth)

    v, _ = ctx.Get(CtxCR)
    cr := v.(*controllers.Crypter)

    auth := strings.Split(strings.TrimSpace(ctx.GetHeader("Authorization")), " ")
    if len(auth) != 2 {
      ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte(errNeedToLogin.Error()))
      ctx.Abort()
      return
    }

    // some parsing error
    jcc := &controllers.JwtCustomClaims{}
    ok, jcc, err := ja.IsValid(auth[1])
    if err != nil {
      switch err.(type) {
      case *jwt.ValidationError:
        ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte(err.Error()))

      default:
        ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      }

      ctx.Abort()
      return
    }

    // jwt is not valid
    if !ok {
      ctx.Data(http.StatusUnauthorized, gin.MIMEPlain, []byte(errNeedToLogin.Error()))
      ctx.Abort()
      return
    }

    userId, err := cr.Decrypt(jcc.UserID)
    // wrong decryption
    if err != nil {
      ctx.Data(http.StatusInternalServerError, gin.MIMEPlain, []byte(err.Error()))
      ctx.Abort()
      return
    }

    ctx.Set("login", fmt.Sprintf("%s %s", jcc.Login, userId))
  }
}

func AbortIfAuth() gin.HandlerFunc {
  return func(ctx *gin.Context) {
    auth := strings.Split(strings.TrimSpace(ctx.GetHeader("Authorization")), " ")

    if len(auth) > 1 {
      ctx.Data(http.StatusBadRequest, gin.MIMEPlain, []byte("need to logout before login/signup. Header \"Authorization\" must be empty."))
      ctx.Abort()
    }
  }
}

func CheckLoginAccuracy() gin.HandlerFunc {
  return func(ctx *gin.Context) {
    v, ok := ctx.Get("login")
    if !ok {
      ctx.Data(
        http.StatusInternalServerError,
        gin.MIMEPlain,
        []byte(`ctx["login"] is empty`),
      )

      ctx.Abort()
      return
    }

    s, ok := v.(string)
    if !ok {
      ctx.Data(
        http.StatusInternalServerError,
        gin.MIMEPlain,
        []byte(`ctx["login"]: value is not string`),
      )

      ctx.Abort()
      return
    }

    a := strings.Split(s, " ")
    if len(a) < 2 || a[0] == "" || a[1] == "" {
      ctx.Data(
        http.StatusInternalServerError,
        gin.MIMEPlain,
        []byte(`ctx["login"]: value has wrong format`),
      )

      ctx.Abort()
      return
    }

    _, err := primitive.ObjectIDFromHex(a[1])
    if err != nil {
      ctx.Data(
        http.StatusInternalServerError,
        gin.MIMEPlain,
        []byte(`ctx["login"]: userId is not Mongo.ObjectId`),
      )

      ctx.Abort()
      return
    }
  }
}
