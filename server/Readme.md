**Build binaries:**

`./build.sh`

**Build docker image:**

`docker build . -t utility:0.0.1`

**Run image:**

`docker run -it --rm -p 9000:9000 utility:0.0.1`
