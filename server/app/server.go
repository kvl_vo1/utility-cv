package app

import (
  "context"
  "flag"
  "fmt"
  "io"
  "log"
  "net/http"
  "os"
  "os/signal"
  "path/filepath"
  "strconv"
  "time"
  "utility/controllers"
  "utility/entity"
  "utility/middlewares"
  "utility/routes"

  "github.com/gin-contrib/cors"
  "github.com/gin-gonic/gin"
)

const (
  defaultRootDir         = "./client"
  defaultUserDataDir     = "/userdata"
  defaultPort        int = 9000
  secretKey              = "some_secret_word"
  defaultDbPath          = "mongodb://localhost:37017/?connect=direct"
)

type App struct {
  params *entity.Params
  ws     *gin.Engine
  jwt    *controllers.JWTAuth
  db     *controllers.MongoDB
  sm     *controllers.SessionMgr
  rm     *controllers.ResetMgr
  crypt  *controllers.Crypter
}

func NewApp() (*App, error) {
  gin.SetMode(gin.ReleaseMode)

  var a = new(App)
  a.params = new(entity.Params)

  _ = a.parseEnv()

  if err := a.setupLogOutput(); err != nil {
    return nil, err
  }

  a.crypt = controllers.NewCrypter()

  db, err := controllers.NewMongoDB(a.params.DbPath, a.crypt)
  if err != nil {
    return nil, err
  }
  a.db = db

  if err := a.initOptions(); err != nil {
    return nil, err
  }

  a.jwt = controllers.NewJWT(a.params.SecretKey)
  a.sm = controllers.NewSessionMan()
  a.rm = controllers.NewResetMan()

  a.initWebServer()
  a.initRoutes()

  return a, nil
}

func (a *App) parseFlags() {
  flag.IntVar(&a.params.Port, "port", defaultPort, "Port")
  flag.StringVar(&a.params.Host, "host", "", "Host")
  flag.StringVar(&a.params.SecretKey, "key", secretKey, "Secret key for token encryption")
  flag.StringVar(&a.params.DbPath, "dbpath", defaultDbPath, "Path to MongoDB")
  flag.StringVar(&a.params.RootDir, "rootpath", defaultRootDir, "Path to front-end client files")
  flag.Parse()
}

func (a *App) parseEnv() error {
  a.params.Host = os.Getenv("UTILITY_HOST")

  v := os.Getenv("PORT")
  if v == "" {
    a.params.Port = defaultPort
  } else {
    if p, err := strconv.Atoi(v); err != nil {
      return fmt.Errorf("parsing error (PORT): %s", err.Error())
    } else {
      a.params.Port = p
    }
  }

  v = os.Getenv("UTILITY_SECRET_KEY")
  if v == "" {
    a.params.SecretKey = secretKey
  } else {
    a.params.SecretKey = v
  }

  v = os.Getenv("UTILITY_DB_PATH")
  if v == "" {
    a.params.DbPath = defaultDbPath
  } else {
    a.params.DbPath = v
  }

  v = os.Getenv("UTILITY_ROOT_PATH")
  if v == "" {
    a.params.RootDir = defaultRootDir
  } else {
    a.params.RootDir = v
  }

  return nil
}

func (a *App) initOptions() error {
  var mkDir = func(dir string) error {
    if _, err := os.Stat(dir); os.IsNotExist(err) {
      if e := os.MkdirAll(dir, os.ModePerm); e != nil {
        return e
      }
    }

    return nil
  }

  o, err := a.db.GetSettings()
  if err != nil {
    return err
  }

  a.params.Options = *o

  dir := filepath.Dir(os.Args[0])
  dir = filepath.Join(dir, a.params.RootDir)

  if err := mkDir(dir); err != nil {
    return err
  }

  a.params.RootDir = dir

  dir = filepath.Join(a.params.RootDir, defaultUserDataDir)
  if _, err := os.Stat(dir); os.IsNotExist(err) {
    if e := os.MkdirAll(dir, os.ModePerm); e != nil {
      return e
    }
  }
  a.params.UserDataDir = dir

  return nil
}

func (a *App) initWebServer() {
  a.ws = gin.New()

  // CORS config
  config := cors.DefaultConfig()
  config.AllowOrigins = []string{"*"}
  config.AllowHeaders = []string{"Origin", "Content-Type", "Accept", "Accept-Encoding", "Content-Length", "Authorization"}
  config.AllowMethods = []string{"GET", "POST", "HEAD", "OPTIONS"}

  // common middlewares
  //a.ws.Use(gin.Logger())
  a.ws.Use(middlewares.Logger())
  a.ws.Use(cors.New(config))
  a.ws.Use(gin.Recovery())

  log.SetOutput(gin.DefaultWriter)
}

func (a *App) initRoutes() {
  a.ws.Use(middlewares.StaticFiles(a.params.RootDir))
  a.ws.NoRoute(routes.NoRoute)

  apiRoutes := a.ws.Group("/api", middlewares.AssignCtx(a.db, a.jwt, a.sm, a.crypt, a.rm, a.params), middlewares.CheckCtx())

  apiRoutes.POST("/signin", middlewares.AbortIfAuth(), routes.SignIn)
  apiRoutes.POST("/signup", middlewares.AbortIfAuth(), routes.SignUp)
  apiRoutes.GET("/signout", routes.SignOut)
  apiRoutes.GET("/reset", routes.Reset)

  apiRoutes.GET("/receipts", middlewares.AuthJWT(), routes.ReceiptsGet)

  receiptsRoutes := apiRoutes.Group("/receipt", middlewares.AuthJWT(), middlewares.CheckLoginAccuracy())
  receiptsRoutes.GET("/:id", routes.ReceiptGet)
  receiptsRoutes.POST("/:id", routes.ReceiptAdd)

  sendRoutes := apiRoutes.Group("/send", middlewares.AuthJWT(), middlewares.CheckLoginAccuracy())
  sendRoutes.POST("/:id", routes.Send)

  filesRoutes := apiRoutes.Group("/files", middlewares.AuthJWT(), middlewares.CheckLoginAccuracy())
  filesRoutes.POST("", routes.SendFiles)
  filesRoutes.POST("/remove", routes.RemoveFile)

  settingsRoutes := apiRoutes.Group("/settings", middlewares.AuthJWT(), middlewares.CheckLoginAccuracy())
  settingsRoutes.GET("", routes.SettingsLoad)
  settingsRoutes.POST("", routes.SettingsSave)
}

func (a *App) setupLogOutput() error {
  f, err := os.OpenFile("./utility.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
  if err != nil {
    return fmt.Errorf("error while opening log file: %s", err.Error())
  }

  gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

  return nil
}

func (a *App) Run() error {
  srv := &http.Server{
    Addr:    a.params.GetAddr(""),
    Handler: a.ws,
  }

  go func() {
    if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
      log.Printf("Error during start server: %s\n", err)
    }
  }()

  // waiting until ctrl+c be pressed
  quit := make(chan os.Signal)
  signal.Notify(quit, os.Interrupt)
  <-quit

  log.Println("Server is shutting down...")

  ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
  defer cancel()
  if err := srv.Shutdown(ctx); err != nil {
    log.Printf("Server is shutdown with error: %s", err)
    return err
  }

  log.Println("Server has been stopped")

  return nil
}
